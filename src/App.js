import React from "react";
import { Route, Switch } from "react-router-dom";
import About from "./pages/About";
import TermsAndConditions from "./pages/TermsAndCondition";
import PrivacyAndPolicy from "./pages/PrivacyPolicy";
import AuthPage from "./pages/Authpage";
import Contact from "./pages/contact page";
import LandingPage from "./pages/Homepage";
import ForgotPassword from "./pages/forgot-password";
import Blog from "./pages/Blog/index";
import MakePayment from "./pages/MakePayment";
import ReviewOrder from "./pages/ReviewOrder/index";
import "./App.css";
import KadunaSouth from "./pages/Restaurant-Page/index";
import KadunaNorth from "./pages/KadunaNorth/index";
import ShoppingCart from "./pages/cart/index";
import RestuarantSinglePage from "./pages/Restaurant single page/index";
import FAQ from "./pages/FAQ";
import UserDashboard from "./pages/UserDashboard";
import RestaurantDashboard from "./pages/RestuarantDashboard";
import AdminBoard from "./pages/ADMIN";
import { connect } from 'react-redux'
import RestuarantSignIn from './pages/Authpage/Forms/restuarant signin'

function App(props) {
 
  return (
    <div>
      
      <Switch>
       
        <Route path="/auth" component={AuthPage} /> 
        <Route exact path="/" component={LandingPage} />
        <Route path="/blog" component={Blog} />
        <Route path="/kaduna-south" component={KadunaSouth} />
        <Route path="/kaduna-north" component={KadunaNorth} />
        <Route path="/contact" component={Contact} />
        <Route path="/forgotpassword" component={ForgotPassword} />
        <Route path="/about" component={About} />
        <Route path="/terms" component={TermsAndConditions} />
        <Route path="/privacy" component={PrivacyAndPolicy} />
        <Route path="/payment" component={MakePayment} />
        <Route path="/cart" component={ShoppingCart} />
        <Route path="/review" component={ReviewOrder} />
        <Route path="/restaurant" component={RestuarantSinglePage} />
        <Route path="/faq" component={FAQ} />
        <Route exact path="/user" component={UserDashboard} />
        <Route
          exact
          path="/profile/dashboard/:token"
          component={RestaurantDashboard}
        />
        <Route  path="/admin" component={AdminBoard} />
        >
        <Route path='/rauth' component={RestuarantSignIn} />
      </Switch>
    </div>
  );
}

const mapStateToProps = state => ({
  userData: state.userReducer
})

export default connect(mapStateToProps, null)(App);
