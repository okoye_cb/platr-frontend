import React from 'react';
import styled from 'styled-components';
import {TextsmsOutlined, EmailOutlined, } from '@material-ui/icons';
import {device} from '../MediaQuery';


const Container = styled.div`
width: 1010px;
height: 454.61px;
border: 1px solid #E3E3E3;
box-sizing: border-box;
margin-left: 70px;
margin-top: 30px;
margin-bottom: 80px;
padding: 30px;
color: rgba(102, 102, 102, 0.4);

@media ${device.phoneGen}{
    margin-top: 160px;
    width: 705px;
}`

const Title = styled.h3`
font-family: Comfortaa;
font-style: normal;
font-weight: normal;
padding: 20px;
font-size: 22px;
line-height: 25px;
color: rgba(102, 102, 102, 0.6)`

const Button = styled.button`
width: 147.8px;
height: 48.21px;
color: #fff;
border: none;
text-align: center;
background: #EB2188`

const Text = styled.p`
font-family: Comfortaa;
font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 20px;
margin-left: 1rem;
color: rgba(102, 102, 102, 0.6);
`
const Block = styled.div`
display: flex;`

const TimeStamp = styled.p`
font-family: Comfortaa;
font-style: normal;
font-weight: bold;
font-size: 14px;
line-height: 12px;
margin-left: 2.5rem;
color: rgba(102, 102, 102, 0.4);`



const Activity = () =>{
    return(
        <Container>
            <Title>Recent Activities</Title>
            <div>
              <Block>
              <TextsmsOutlined/><Text>  A new restaurant is pending your approval</Text> 
              </Block>
              <TimeStamp>6 min ago</TimeStamp> 
            </div>
<div>
<Block>
<EmailOutlined/><Text> You Messaged Ajegunle Finest Chao! “Why is the order taking so long? It has been more than an hour now please make it snappy”</Text></Block>  
<TimeStamp>19 min ago</TimeStamp>
</div>

<div>
<Block>
    <TextsmsOutlined/><Text> You replied to a comment in the review section “Why is the order taking so long? It has been more than an hour now please make it snappy”</Text>
</Block>
    <TimeStamp>6 min ago</TimeStamp> 
</div>

            <div style={{textAlign: 'center', }}>
            <Button>View All</Button>
            </div>
        </Container>
    )
}


export default Activity;