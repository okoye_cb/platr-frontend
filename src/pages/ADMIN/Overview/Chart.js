import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, Tooltip,
} from 'recharts';
import styled from 'styled-components';
import {device} from '../MediaQuery';


const Container = styled.div`
@media ${device.phoneGen}{
  LineChart{
    width: 200;
  }
}`
const data = [
  {
    name: 'Jan 1', pv: 2400
  },
  {
    name: 'Jan 2', pv: 1398 
  },
  {
    name: 'Jan 3', pv: 3300
  },
  {
    name: 'Jan 4', pv: 3908 
  },
  {
    name: 'Jan 5', pv: 4200
  },
  {
    name: 'Jan 6', pv: 3800 
  },
  {
    name: 'Jan 7', pv: 3300 
  },
  {
    name: 'Jan 8', pv: 4000 
  },
  {
    name: 'Jan 9', pv: 2300 
  },
  {
    name: 'Jan 10', pv: 4300 
  },
  {
    name: 'Jan 11', pv: 3800 
  },
  {
    name: 'Jan 12', pv: 2500 
  },
];

export default class Example extends PureComponent {

  render() {
    return (
      <Container>
        <LineChart
        width={650}
        height={400}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        
        <XAxis dataKey="name" />
        <YAxis datakey="pv" />
        <Tooltip />
        <Line type="monotone" dataKey="pv" stroke="#EB2188" activeDot={{ r: 8 }} />
      </LineChart>
      </Container>
    );
  }
}
