import React from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEllipsisH} from '@fortawesome/free-solid-svg-icons'
import Chart from './Chart'
import {device} from '../MediaQuery'


const Container = styled.div`
width: 1010px;
height: 514.61px;
border: 1px solid #E3E3E3;
box-sizing: border-box;
margin-left: 70px;
margin-top: -130px;

@media ${device.phoneGen}{
    margin-top: 160px;
    width: 705px;
}
`

const NormalText = styled.p`
font-family: Comfortaa;
font-style: normal;
font-weight: normal;
font-size: 22px;
line-height: 25px;
padding: 20px;
color: rgba(102, 102, 102, 0.6);`


const Block = styled.div`
display: flex;
justify-content: space-between`


const Select = styled.select`
width: 171.64px;
height: 30.25px;
border: 1px solid #E3E3E3;
box-sizing:
font-family: Comfortaa;
font-style: normal;
font-weight: bold;
font-size: 14px;
line-height: 18px;
float: right;
margin-right: 50px;
color: rgba(102, 102, 102, 0.6);`




const Graph = () =>{
    return(
<Container>
    <Block>
        
<NormalText>Number of Visits</NormalText>
<FontAwesomeIcon icon={faEllipsisH} color= "#E3E3E3" style={{marginRight: '40px', marginTop: '20px'}}/>
    </Block>

    <Select>
        <option value="date">12th January 2019</option>
    </Select>

    <Chart />
</Container>
    )
}


export default Graph;