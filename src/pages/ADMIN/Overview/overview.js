import React from 'react';
import styled from 'styled-components';
import Topview from './Topview';
import Graph from './Graph';
import Activity from './Activity';



const Wrapper = styled.div`
  width: 100%;
  height: 95%
`
const Container = styled.div`
    height: 90%;
    width: auto
    
   
`


const Overview = () =>{
    return(
  <Container>
<Wrapper>
<Topview/>
<Graph/>
<Activity/>
</Wrapper>
  </Container>
           
    
    )
}



export default Overview;