import React from 'react';
import styled from 'styled-components';
import money from '../../../money.svg';
import invoice from '../../../invoice.svg'
import {device} from '../MediaQuery'


const Container = styled.div`
width: auto;
height: 30%;
display: flex;
margin-top: 2rem;
margin-left: 30px;

@media ${device.phoneGen}{
flex-flow: column wrap;
justify-content: space-between;
}
`



const Block = styled.div`
width: 311px;
height: 93.02px;
background: #FFFFFF;
border: 1px solid rgba(189, 189, 189, 0.25);
box-sizing: border-box;
box-shadow: 0px 4.71062px 23.5531px rgba(0, 0, 0, 0.07);
border-radius: 4.71062px
padding: 20px;
margin-left:40px;
display: flex`

const Text = styled.h4`
font-family: Comfortaa;
font-style: normal;
font-weight: bold;
font-size: 10px;
line-height: 11px;
text-transform: uppercase;
color: #BDBDBD`

const Price = styled.h3`
font-family: Comfortaa;
font-style: normal;
font-weight: bold;
font-size: 24px;
width:149px;
line-height: 27px;
color: #2D74DA`

const Icon = styled.div`
width: 65.13px;
height: 65.13px;
margin-left: 3.8rem
background: ${props => props.invoice ? "#1C75BB" : props.money? "#EB2188" : "#FFBA49"};
border-radius: 4.71px;
margin-top: -8px;
`

const Img = styled.img`
padding: 15px;
`

const Topview = (props) =>{
    return(
        <Container>

            <Block>
           <div>
           <Text>
               Total restuarants
           </Text>   
           <Price>72, 000</Price>
           </div>
           <Icon invoice>
           <Img src={invoice} alt="invoice"/>
           </Icon>
           </Block>


           <Block>
           <div>
           <Text>
               Total users
           </Text>   
           <Price>1, 700, 000</Price>
           </div>
           <Icon money>
               <Img src={money} alt="money"/>
           </Icon>
           </Block>


           <Block>
          <div>
          <Text>
               Total turnover
           </Text>   
           <Price>#600,  000.00</Price>
          </div>
           <Icon>
               <Img src={money} alt="money"/>
           </Icon>
           </Block>
           
        </Container>
    )
}


export default Topview;