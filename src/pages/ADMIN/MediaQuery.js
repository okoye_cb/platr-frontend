const size = {
   mobileS: '320px',
   mobileM: '375px',
   mobileL: '425px',
   tablet: '768px',
   laptop: '1024px',
   BigScreen:'1440px'
}

export const device = {
  mobileS : `(min-width:${size.mobileS})`,
  mobileM: `(min-width: ${size.mobileM})`,
  mobileL: `(min-width: ${size.mobileL})`,
  phoneGen: `(min-width: ${size.mobileS}) and (max-width: ${size.mobileL})`,
  laptop:   `(min-width: ${size.laptop})`,
  BigScreen: `(min-width: ${size.BigScreen})`
}