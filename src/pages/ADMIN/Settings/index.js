import React, { useState } from 'react'
import styled from 'styled-components'
import CustomButton from '../../../components/Button'
import CustomizedSwitches from './Toggle/toggle'

const Setting = styled.section`
  display: flex,
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 4rem 0
`

const ProfileTab = styled.div`
  border: 1px solid grey;
  width: 90%;
  margin: 1rem auto;
  padding : .75rem;
  display: flex;
  justify-content: space-between;
  align-items: center
`
const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

`

const Input = styled.input`
  width: 90%;
  padding: 0.5rem;
  margin: 1rem auto;
  position: relative
`


const Media = styled.section`
  width: 90%;
  margin: 0.65rem auto;
  ul {
    li{
      display: flex;
      align-items: center;
    }
  }
`

const Turndown = (toggle) => {
  const s = document.querySelectorAll('.arr')
  s.forEach( ( arr, i )=> {
      
      arr.addEventListener('click', () => {
      if (!toggle) {
        arr.style.transform = 'rotate(180deg)'
        arr.style.marginTop = '-5px'
      } else {
        arr.style.transform = 'rotate(0deg)'
        arr.style.marginTop = '5px'
      }

  })
})
}

const Settings = () => {
 const [Unhide, Onhide] = useState(false)
 const [Unhide1, Onhide1] = useState(false)
 const [Unhide2, Onhide2] = useState(false)
  return (
    <div>
      <Setting>
        <ProfileTab  onClick={ () => Onhide(!Unhide, Turndown(!Unhide)) }>
          <span>Profile Information</span>
          <span className='arr' 
          style={{marginTop:'10px', cursor: 'pointer'}}
          onClick={ () => Onhide(!Unhide, Turndown(!Unhide)) }
          >&#94;</span>
        </ProfileTab>
        {
          Unhide ?
          
            <Form>
              <Input type='text' placeholder='Admin Name' required/>
              <Input type='number' placeholder='Phone Number' required/>
              <Input type='text' placeholder='Address'/>
              <Input type='email' placeholder='Work Email' required/>
              <div style={{width:'90%', margin:'0.3rem auto'}}>
                <CustomButton size='lg' text='Update' type='submit'/>
              </div>
            </Form> : ''
        }
      </Setting>

      <Setting>
      <ProfileTab onClick={ () => Onhide1(!Unhide1, Turndown(Unhide1))}>
          <span>Media Notification</span>
          <span className='arr' style={{marginTop:'10px', cursor: 'pointer', fontSize:'1rem'}}
          onClick={ () => Onhide1(!Unhide1, Turndown(Unhide1))}
          >&#94;</span>
        </ProfileTab>
        <Media>
          {
            Unhide1 ?  
          <ul>
            <li >
              <span style={{padding:'0 1rem'}}>LinkedIn</span>
              <section style={{paddingLeft: '0.9rem' }}>
                <CustomizedSwitches />
              </section>
            </li>
            <li >
              <span style={{padding:'0 1rem'}}>FaceBook</span>
              <section style={{paddingLeft: '0.2rem' }}>
                <CustomizedSwitches />
              </section>
            </li>
            <li >
              <span style={{padding:'0 1rem'}}>Instagram</span>
              <CustomizedSwitches />
            </li>
            <li >
              <span style={{padding:'0 1rem'}}>Twitter</span>
              <section style={{paddingLeft: '1.7em' }}>
              <CustomizedSwitches />
              </section>
            </li>
            <li >
              <span style={{padding:'0 1rem'}}>Email</span>
              <section style={{paddingLeft: '2.2em' }}> 
               <CustomizedSwitches />
              </section>
            </li>
        <div style={{ margin:'0.3rem auto'}}>
                <CustomButton size='lg' text='Update' type='submit'/>
              </div>
          </ul>

          :
          ''
          }
        </Media>
      </Setting>

      <Setting>
        <ProfileTab onClick={ () => Onhide2(!Unhide2, Turndown(Unhide2))}>
          <span>Change Password</span>
          <span className='arr' style={{marginTop:'10px', cursor: 'pointer'}}
           onClick={ () => Onhide2(!Unhide2, Turndown(Unhide2))}>&#94;</span>
        </ProfileTab>
        {
          Unhide2 ?
            <Form>
              <Input type='text' placeholder='Old Password' required/>
              <Input type='number' placeholder='New Password' required/>
              <Input type='text' placeholder='Confirm New Password'/>
              <Input type='email' placeholder='Password Recovery Email' />
              <div style={{width:'90%', margin:'0.3rem auto'}}>
                <CustomButton size='lg' text='Update' type='submit'/>
              </div>
            </Form> : ''
        }
      </Setting>

    </div>
  )
}

export default Settings
