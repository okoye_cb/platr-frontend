import React from 'react'
import styled from 'styled-components'
import { device } from '../MediaQuery'
import { getAllRestuarant } from '../../../redux/Admin/Restuarants/Restuarants'
import { connect } from 'react-redux'

const Container = styled.div`
  margin: 2rem auto;
  width: 80%;
  display: flex;

  @media ${device.phoneGen}{
    flex-direction: column;
    width:85%
  }
  
  span{
    border: 1px solid #ccc;
    border-right: 1px solid #ccc;
    padding: 1rem 2rem;
    cursor: pointer;
 
    
    &:hover {
      color: white;
      background: deeppink
      transition: all 0.3s ease-in;
      
      &:before {
        width: 100%;
      }
    }
              span:last-child{
                border-right: 1px solid #ccc
              }
    
    @media ${device.phoneGen}{
      border-right: 1px solid #ccc
    }
  }
  span:nth-child(5){
    border-right: 1px solid #ccc
  }
`
const Restaurant = (props) => {
  return (
    <Container>
      <span onClick={() => props.getAllRestuarant()}>All Restuarant</span> 
      <span onClick={() => props.getAllRestuarant('active')}>Active</span> 
      {/* <span onClick={() => props.getAllRestuarant('inactive')}>Inactive</span>  */}
      <span onClick={() => props.getAllRestuarant('pending')}>Pending Approval</span>
      <span onClick={() => props.getAllRestuarant('rejected')}>Rejected Restaurant</span>
    </Container>
  )
}

export default connect(null, { getAllRestuarant })(Restaurant)
