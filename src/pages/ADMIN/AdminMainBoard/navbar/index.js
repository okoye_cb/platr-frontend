import React from 'react'
import styled from 'styled-components'
import { device } from '../../MediaQuery'

const Container =styled.div`
  display:flex;
  align-items:center;
  justify-content: space-between;
  width:100%;
  background: #FFFFFF;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  height: 4rem;
  padding: 0 1.5rem;
  overflow-x: hidden

  @media ${device.phoneGen}{
    flex-direction: column;
    justify-center: center;
    align-items: center;
    overflow-x: hidden
  }
`
const Wrapper = styled.div` width: 100% `

const Span = styled.span`

   span{
     margin-right:.7rem
     text-transform: capitalize;
     font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 27px;
    color: #666666;
   }
`
const Select = styled.select`
   border: none
`
const Admin = localStorage.getItem('admin')
const AdminPersonale = JSON.parse(Admin)

const Nav = ({ name }) => {
  return (
    <Wrapper>
      <Container>
        <Span>
          <span>&larr;</span><span>{name}</span>
        </Span>

        <Select>
          <option value='Mfonobong'>{`${AdminPersonale.userDetails.firstName}  ${ AdminPersonale.userDetails.lastName}` }</option>
          <option value='Mfonobong'>Mfonobong Umondia</option>
          <option value='Mfonobong'>Mfonobong Umondia</option>
          <option value='Mfonobong'>Mfonobong Umondia</option>
          <option value='Mfonobong'>Mfonobong Umondia</option>
        </Select>
    
      </Container>
 
      {/* <Restuarant /> */}
     </Wrapper >
      
      
    
  )
}

export default Nav
