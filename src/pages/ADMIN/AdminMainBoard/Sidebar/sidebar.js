import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendar, faCalendarMinus, faCog, faUserAlt } from '@fortawesome/free-solid-svg-icons'
import { device } from '../../MediaQuery'
import {getAllRestuarant} from '../../../../redux/Admin/Restuarants/Restuarants'
import {connect }  from  'react-redux'

const Container = styled.div`
  background: #EB2188;
  height: 100vh;
  position: -webkit-sticky;
  position: sticky;
  top:0;
  overflow-y:  hidden;
  width: 20vw;
  display: flex;
  flex-direction: column;
  align-items: center

`
const Text = styled.h4`
  
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 27px;
  text-transform:capitalize;
  color: #FFFFFF;
  height: 2rem;
  width: 100%;
  margin-top:1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 4rem;

  @media ${device.phoneGen} {
   font-size: 1rem;
   justify-content: center,
   width: 100%;
   margin-left: 4rem
  }
`

const List = styled.ul`
  list-style: none;
  margin-top: 2rem;

  @media ${device.phoneGen}{
    margin-top: 1rem
  }

  li{
    margin: 2rem 0;
    color: white;
    display:block;
    text-transform: capitalize;

    span{
      font-size: 1.5rem;
      font-style: normal;
      font-weight: normal;
      font-size: 20px;
      line-height: 22px;
      color: #FFFFFF;
      padding-left:1rem;
      cursor: pointer

      @media ${device.phoneGen}{
        display:none
      }
    }
  }
  

`

const Sidebar = ({props}) => {
  
  const click = (props) => {
  props.history.push(props.match.path + '/restuarants')
  // props.getAllRestuarant()
  }
  return (
    <Container>
      <Text >
         Admin
      </Text>

      <List>
        <li onClick={() => props.history.push(props.match.path + '/overview')}>
          <FontAwesomeIcon size='lg' icon={faCalendar} color='white' />   <span> overview </span>
        </li>
        <li onClick={() => click(props)}>
          <FontAwesomeIcon size='lg' icon={faCalendarMinus} color='white' />  <span>restuarants</span>
        </li>
        <li onClick={() => props.history.push(props.match.path + '/staffs')}>
          <FontAwesomeIcon size='lg' icon={faUserAlt} color='white' />  <span>staffs</span>
        </li>
        <li onClick={() => props.history.push(props.match.path + '/settings')}>
          <FontAwesomeIcon size='lg' icon={faCog} color='white' />  <span>settings</span>
        </li>
      </List>
    </Container>
  )
}

export default connect(null, { getAllRestuarant })(Sidebar)
