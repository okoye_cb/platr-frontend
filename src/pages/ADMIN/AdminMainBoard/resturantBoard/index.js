import React, { useState } from 'react'
import styled from 'styled-components'
import Restaurant from '../../Resturants'
import './resturantByStatus.scss'
import RestuarantByStatus from './resturantByStatus';
import { getAllRestuarant } from '../../../../redux/Admin/Restuarants/Restuarants'
import { connect } from 'react-redux'


const Wrapper = styled.div`
  width: 100%;
  height: 95%
`

const Container = styled.div`
  border: 1px solid #ccc;
  width: 70vw;
  height:75%;
  margin: 0 auto;
  background: #FFFFFF;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.25);
  border-radius: 3px
`

const Table = styled.table` 
  width: 100%

`
const TR = styled.tr`
  display: flex;
  justify-content: space-between;
  padding: 0 2rem;
  border-bottom: 1px solid #ccc;
  
  th, td{
    padding: 1rem
  }
`

const Nav = styled.div`
    
    width: 70vw;
    margin: 1rem auto;
    display: flex;
    justify-content: space-between

`


const Board = (props) => {
  const [page, pageCount] = useState({ start: 1 })

  const next = () => {
    pageCount({
      start: page.start + 1
    })
    props.getAllRestuarant(undefined, page.start);
  };

  const prev = () => {
    pageCount({
      start: page.start - 1
    })
    props.getAllRestuarant(undefined, page.start);
    
  };
  return (
    <Wrapper>
      <Restaurant />
      <Container>
        <Table>
          <TR>
            <th>Name of Resturant</th>
            <th>status</th>
          </TR>
          <RestuarantByStatus />
        </Table>
      </Container>
      <Nav>
        {
          page.start !== 1 ?
            <span onClick={() => prev()}><span>🡰 </span>Previous</span> : ''
        }
        <span onClick={() => next()}>Next<span> 🡲</span></span>
      </Nav>
    </Wrapper>
  )
}

export default connect(null, { getAllRestuarant })(Board)
