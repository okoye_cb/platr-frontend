import React, { Component } from 'react'
import styled from 'styled-components';
// import './resturantByStatus'
import MoreVertOutlined from "@material-ui/icons/MoreVertOutlined";
import StatusModal from '../statusModal'
import { connect } from 'react-redux'


const TR = styled.tr`
  display: flex;
  justify-content: space-between;
  padding: 0 2rem;
  border-bottom: 1px solid #ccc;
  
  th, td{
    padding: 1rem
  }
`

//  const RestuarantByStatus = () => {
class InActive extends Component {
  state = {
    vendors: [],
    active: [],
    inActive: [],
    pending: [],
    rejectedResturants: [],
    statusModal: false,

  }

  showStatusModal = (id) => {
    this.setState({
      ...this.state,
      statusModal: {
        [id]: true
      }

    })
  }


  Vendors = () => {
    this.setState({
      ...this.state,
      vendors: this.props.vendors
    })
  }

  render() {
    return (
      <>

        {
        this.props.vendors.vendors.map(vendor => 
               
           (
            <TR key={vendor.id}>
              <td>{vendor.businessName + ' -' + (vendor.Address?.region || 'update location')}</td>
              
              <td> {vendor.status}
                <span style={{ position: 'relative' }} onClick={this.showStatusModal.bind(this, vendor.id)}><MoreVertOutlined /></span>
                {
                  !this.state.statusModal ? '' :
                    <span style={{ position: 'absolute', marginTop: '-2rem' }} ><StatusModal isOpen={this.state.statusModal[vendor.id]}
                    vendorId={vendor.id}
                    /></span>
                }
              </td>
            </TR>

          )
          )
        }
      </>
    )
    }

  }
// }
const mapStateToProps = (state) => ({
  vendors: state.AdminReducer
})

export default connect(mapStateToProps, null)(InActive)