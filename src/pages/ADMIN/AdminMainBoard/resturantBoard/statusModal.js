import React, { Component } from 'react'
import styled from 'styled-components'
import { updateStatus } from '../../../../redux/Admin/Restuarants/Restuarants'
import { connect } from 'react-redux'


const modalStyle = {
  width: '100px',
  height: 'max-content',
  backgroundColor: 'whitesmoke',
  padding: '0.45rem 0',
  textAlign: 'start',
  background: '#FFFFFF',
  boxShadow: '0px 0px 5px rgba(16, 112, 177, 0.2)',
  borderRadius: '3px',
  listStyle: 'none',
  cursor: 'pointer'


};

const Hover = styled.ul`
  display: block

  li {
    &:hover {
      color: white;
      background: deeppink
      transition: all 0.3s ease-in;
  
    &:before {
        width: 100%;
    }
  }

`


export class StatusModal extends Component {
  constructor(props) {
    super()
    this.state = {
      show: true
    }

  }

  render() {

    return (
      <>
        {
          this.props.isOpen ?
            <div style={modalStyle}>
              <Hover style={{ listStyle: 'none' }} isOpen={this.props.isOpen}>
                <li style={{ padding: '0.2rem 0.4rem' }}
                  onClick={updateStatus(
                    {
                      id: this.props.vendorId,
                      updateTo: 'active'
                    })}
                >Accept</li>
                <li style={{ padding: '0.2rem 0.4rem' }}
                  onClick={updateStatus(
                    {
                      id: this.props.vendorId,
                      updateTo: 'inactive'
                    })}
                >Deactivate</li>
                <li style={{ padding: '0.2rem 0.4rem' }}
                  onClick={updateStatus(
                    {
                      id: this.props.vendorId,
                      updateTo: 'rejected'
                    })}
                >Reject</li>

              </Hover>
            </div> : !this.state.show ? '' :
              ''
        }


      </>
    )
  }
}

export default connect(null, { updateStatus })(StatusModal)
