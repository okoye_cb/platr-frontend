import React, { Component } from 'react'

import { connect } from 'react-redux'
// import InActive from './resStatus/inactive'
import Active from './resStatus/active'
import Rejected from './resStatus/rejectedresturants'
import AllRes from './resStatus/allresturant'


//  const RestuarantByStatus = () => {
class RestuarantByStatus extends Component {
  state = {
    vendors: [],
    active: [],
    inActive: [],
    pending: [],
    rejectedResturants: [],
    statusModal: false,

  }

  showStatusModal = (id) => {
    this.setState({
      ...this.state,
      statusModal: {
        [id]: true
      }

    })
  }


  Vendors = () => {
    this.setState({
      ...this.state,
      vendors: this.props.vendors
    })
  }

  render() {
  
    const component = {
      0: <AllRes />,
      1: <Active />,
      // 2: <InActive />,
      2: <Rejected/>
    }

    const resStatus = [ 'allrestuarant', 'active',  'rejected'];

    const display = () => {
      let cur = '';
      let prev =''

      resStatus.forEach( ( status, i ) => {
        for( let key of  Object.keys(component)) {
          prev =  status
          if( Number(key) === i && prev ) {
              prev ? cur = component[key] : cur = component
          }
        }
      } )
        return cur
    }
    return (
      <>
        {display()} 
      </>
    )

  }
}
const mapStateToProps = (state) => ({
  vendors: state.AdminReducer
})

export default connect(mapStateToProps, null)(RestuarantByStatus)