import React from 'react'
import styled from 'styled-components'
import Sidebar from './AdminMainBoard/Sidebar/sidebar'
import Nav from './AdminMainBoard/navbar'
import Board from './AdminMainBoard/resturantBoard'
import Overview from './Overview/overview'
import { device } from './MediaQuery'
import Settings from './Settings'

const Container = styled.div`
  display:flex;
  
  @media ${device.phoneGen}{
    flex-direction: column !important,
    background: red
  }

`
const components = {
  0 : <Overview /> ,
  1 : <Board /> ,
  2 : 'Staff',
  3 : <Settings />  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  
}

const text = [ 'overview', 'restuarants', 'staffs', 'settings' ]

const AdminBoard = ( props) => {
  const getComponent = () => {
    let funcUniversal  = '';
    let component =''
    text.forEach( ( item, i ) => {
      for ( let key of Object.keys(components) ) {
        funcUniversal = props.location.pathname === props.match.path + '/' + item 
        if( Number(key) === i  && funcUniversal ) {
            funcUniversal ? component = components[key] : component = component;
        }}
    })
    return component 
  }

  const getText = () => {
      let title, navTitle ;
      const newText = [...text]
      newText.forEach(( item, i ) => {
        title = props.location.pathname === props.match.path + '/' + item
        if ( title ) {
          title ? navTitle = item.charAt(0).toUpperCase() + item.slice(1) :  navTitle = ''
        }
      })
      return navTitle
  }

  return (
    <Container>
      <Sidebar  props={props}/>
      <div style={{width: "100%"}}>
        <Nav name= { getText() } />
        {getComponent()}
      </div>
    </Container>
  )
}

export default AdminBoard

