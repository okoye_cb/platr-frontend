import React from 'react';
import Header from './Header'
import AccordionSect from './Accordion'
import Footer from '../../components/Footer'

// import './faq.scss'

const FAQ = () => (
  <div>
    <Header />
    {/* <div style={{border: '1px solid transparent', fontSize:'4rem',display:'flex', justifyContent:'center', alignItems:'center', height:'100vh'}}>
      Chidima is so damn cute!!!!!!
    </div> */}
    <AccordionSect />
    <Footer />
  </div>
)

export default FAQ