import React from 'react';
import Sidebar from './component/sidebar/sidebar'
import Accordion from './component/sidebar/accordion'

import './faq.scss'

const AccordionSect = () => (
  <div className='mid'>
    <Sidebar />
    <Accordion />
  </div>
)

export default AccordionSect