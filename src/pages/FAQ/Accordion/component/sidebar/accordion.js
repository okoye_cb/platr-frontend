import React from 'react'


import './accordion.scss'

const Accordion = () => (
  <div className='accordion arrows'>
       {/* <nav class="accordion arrows"> */}
		<header className="head">
      {/* <h2>General</h2> */}
			<label for="acc-close" className="t">General</label>
		</header>
		<input type="radio" name="accordion" id="cb1" />
		<section className="box">
			<label className="box-title" for="cb1">Random text</label>
			<label className="box-close" for="acc-close"></label>
			<div className="box-content">Click on an item to open. Click on its header or the list header to close.</div>
		</section>
		<input type="radio" name="accordion" id="cb2" />
		<section className="box">
			<label className="box-title" for="cb2">Random too</label>
			<label className="box-close" for="acc-close"></label>
			<div className="box-content">Add the className 'arrows' to nav.accordion to add dropdown arrows.</div>
		</section>
		<input type="radio" name="accordion" id="cb3" />
		<section className="box">
			<label className="box-title" for="cb3">Some other random text</label>
			<label className="box-close" for="acc-close"></label>
			<div className="box-content">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque finibus tristique nisi, maximus ullamcorper ante finibus eget.</div>
		</section>
		<input type="radio" name="accordion" id="cb4" />
		<section className="box">
			<label className="box-title" for="cb4">Some other random text</label>
			<label className="box-close" for="acc-close"></label>
			<div className="box-content">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque finibus tristique nisi, maximus ullamcorper ante finibus eget.</div>
		</section>
		<input type="radio" name="accordion" id="cb5" />
		<section className="box">
			<label className="box-title" for="cb5">Lorem Lorem Lorem</label>
			<label className="box-close" for="acc-close"></label>
			<div className="box-content">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque finibus tristique nisi, maximus ullamcorper ante finibus eget.</div>
		</section>
		<input type="radio" name="accordion" id="cb6" />
		<section className="box">
			<label className="box-title" for="cb6">Ipsum Ipsum Ipsum</label>
			<label className="box-close" for="acc-close"></label>
			<div className="box-content">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque finibus tristique nisi, maximus ullamcorper ante finibus eget.</div>
		</section>
		<input type="radio" name="accordion" id="cb7" />
		<section className="box">
			<label className="box-title" for="cb7">Lorem Ipsum Lorem Ipsum</label>
			<label className="box-close" for="acc-close"></label>
			<div className="box-content">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque finibus tristique nisi, maximus ullamcorper ante finibus eget.Lorem Lorem Lorem</div>
		</section>

		<input type="radio" name="accordion" id="acc-close" />
	{/* </nav> */}
  </div>
)

export default Accordion

