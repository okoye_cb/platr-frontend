import React from 'react'
import './sidebar.scss'

const Sidebar = () => (
  <>
  <div className='sidebar'>
    <h4>General</h4>
    <ul className='list'>
      <li className='item'>Random text</li>
      <li className='item'>Random text</li>
      <li className='item'>Random text</li>
      <li className='item'>Random text</li>
      <li className='item'>Random text</li>
      <li className='item'>Random text</li>
      <li className='item'>Random text</li>
    </ul>
  </div>
  </>
)


export default Sidebar