import React from 'react'
import MainHeader from '../../../components/PagesHeader/MainHeader'
import Navigation from '../../../components/Header/newNavBar'
import TitleAndDescription from '../../../components/PagesHeader/Title'
import platrwhite from "../../../platr.svg"

import "bootstrap/dist/css/bootstrap.css";

const Header = () => (
  <div className='faq'>
     <MainHeader 
      renderProp={
        () => (
          <>
            <Navigation 
              idName={"pink-color"}
              brand={platrwhite}
              iconColor="icon-pink"
              bgColor="white"
              
            />
            {/* className give margin-top on desktop and remove on mobile */}
            <div className='title'style={{ marginTop:'7rem'}}>
              <TitleAndDescription title={"FAQ"} />
            </div>
          </>
        )
      }
     />
  </div>
)

export default Header;