import React from 'react';
import './total.style.scss';
import { connect } from 'react-redux';
import { selectCartTotal } from '../../../redux/cartReducer/cart.selectors'



const Total = ({total}) =>{
    return(
        
            <div className="grand--total ">
     <h3 className="grand--total__price">Grand Total: &#x20A6; {total}</h3>
     <p className="grand--total__text">Total includes delivery fee</p>

   </div>
     
    )
}
const mapStateToProps = (state) => ({
  total: selectCartTotal(state)
});


export default connect(mapStateToProps) (Total);