const CartData = [
  {
    restaurantName: "Pepe's Restaurant",
    productImage:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1582100690/Rectangle_4.3_uo2sli.png",
    productID: 12345678910,
    productName: "Yam Chips and Burger",
    productDescription: "Yam, minced meat and italian herbs.",
    productPrize: 1500,
  },
  {
    restaurantName: "Pepe's Restaurant",
    productImage:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1582100690/Rectangle_4.3_uo2sli.png",
    productID: 12345678910,
    productName: "Yam Chips and Burger",
    productDescription: "Yam, minced meat and italian herbs.",
    productPrize: 1500,
  },
  {
    restaurantName: "Pepe's Restaurant",
    productImage:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1582100690/Rectangle_4.3_uo2sli.png",
    productID: 12345678910,
    productName: "Yam Chips and Burger",
    productDescription: "Yam, minced meat and italian herbs.",
    productPrize: 1500,
  },
];

export default CartData;
