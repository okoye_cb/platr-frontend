import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus, faTimes } from "@fortawesome/free-solid-svg-icons";
import "./cart.style.scss";


import { connect } from 'react-redux';
import { clearItemFromCArt, addItem, removeItem } from '../../../redux/cartReducer/cart-actions'

const CartItem = ({ item, clearItem, addItem, removeItem }) => {
  const { id, imgSrc, desc, delivery, price, menu, quantity } = item;
  return (
    <div className="container cart-wrap">
       <div className="product">
      {/* product info section */}
      <div className="product--detail">
        <h2 className="product-title title">Product Name & Details</h2>
        <div className="product--details">
          <img
            src={imgSrc}
            className="img-responsive product-image"
            alt="productimage"
          />
          <div className="product--info">
            <h5 className="product--info__name">{menu}</h5>
            <p className="product--info__description">
              {desc}
            </p>
            <p className="product--info__id">ID: {id}</p>
            <h3 className="product--info__remove" onClick={() => clearItem(item)}>
              {" "}
              <FontAwesomeIcon icon={faTimes} /> Remove
            </h3>
          </div>
        </div>
      </div>

      {/* Here is the quantity counter */}
      <div className="quantity--detail">
        <h2 className="quantity--detail__title title">Quantity</h2>
        <div className="quantity--detail__counter">
        <FontAwesomeIcon
            icon={faMinus}
            onClick={() => removeItem(item)}
            className="cursorType mt-2"
          />
          <span className="quantity--detail__quantities">
            {quantity}{" "}
          </span>
          <FontAwesomeIcon
            icon={faPlus}
            onClick={() => addItem(item)}
            className="cursorType mt-2"
          />
        </div>
      </div>

      {/* Price section */}
      <div className="price--details">
        <h2 className="price--details__title title">Price</h2>
        <p className="price--details__amount">
          &#x20A6; {price}
        </p>
      </div>

      {/* Delivery section */}
      <div className="delivery--details">
        <h2 className="delivery--details__title title">Delivery Details</h2>
        <p className="delivery--details__time">
          Delivery time: 45min - 1hr
        </p>
        <p className="delivery--details__fee">Delivery fee:&#x20A6; { quantity * delivery}</p>
      </div>

      {/* total    */}
    </div>
    <h3 className="product-total">Total: { quantity * price}</h3>
  </div>
);

}
const mapDispatchToProps = dispatch => ({
  clearItem: item => dispatch(clearItemFromCArt(item)),
  addItem: item => dispatch(addItem(item)),
  removeItem: item => dispatch(removeItem(item))
})

export default connect(null, mapDispatchToProps) (CartItem);