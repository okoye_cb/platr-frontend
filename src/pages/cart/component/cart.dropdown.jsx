import React from 'react';
import CustomButton from '../../../components/Button/index'
import './cart-dropdown.scss';


const CartDropdown = () =>(
    <div className="cart-dropdown">
        <div className="cart-items" />
        <CustomButton text='Checkout' size="lg"  />
    </div>
)



export default CartDropdown;



const Cart = ({ cartItems }) => (
    <div className='cart-dropdown'>
      <div className='cart-items'>
        {cartItems.map(cartItem => (
          <CartItem key={cartItem.id} item={cartItem} />
        ))}
      </div>
    
    </div>
  );
  
  const mapStateToProps = ({ cart: { cartItems } }) => ({
    cartItems
  });
  
  export default connect(mapStateToProps) (Cart);