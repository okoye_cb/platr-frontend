import React from "react";

import CartItem from './cart-item';
import { connect } from 'react-redux';
import { selectCartItems } from '../../../redux/cartReducer/cart.selectors'


const Cart = ({ cartItems }) => (
  
    <div className='cart-items'>
      {cartItems.map(cartItem => (
        <CartItem key={cartItem.id} item={cartItem} />
      ))}
    </div>
  
);

const mapStateToProps = (state) => ({
  cartItems: selectCartItems(state)
});

export default connect(mapStateToProps) (Cart);