import React from "react";
import Navigation from "../../components/Header/newNavBar";
import platr from "../../platr.svg";
import "./index.style.scss";
import Cart from "./component/Cart";
import Footer from "../../components/Footer";

import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import {
  selectCartItemsCount,
  selectCartTotal,
} from "../../redux/cartReducer/cart.selectors";

import { Link } from "react-router-dom";

const ShoppingCart = ({ itemCount, total }) => {
  return (
    <div className="container-fliud">
      <div style={{ background: "deeppink", width: "auto" }}>
        <div className="container-fliud">
          <Navigation
            // name={"Menus"}
            idName="pink-color"
            brand={platr}
            iconColor="icon-pink"
            bgColor="white"
          />
        </div>
      </div>
      <div className="container">
        <div className="shopping--cart container">
          <h2 className="shopping--cart__title">
            Shopping cart{" "}
            <span className="shopping--cart__counter">
              ({itemCount ? `${itemCount} items` : "Empty"})
            </span>
          </h2>
          <div className="shopping--cart__flex">
            <Link to="/restaurant">
              <p className="shopping--cart__subhead">Continue shopping</p>
            </Link>

            <p className="shopping--cart__emptycart">
              {itemCount ? `${itemCount} Item Added to Cart` : `Empty cart`}
            </p>
          </div>
        </div>
        <Cart />
        <div className="grand--total ">
          <h3 className="grand--total__price">Grand Total: &#x20A6; {total}</h3>
          <p className="grand--total__text">Total includes delivery fee</p>

          <Link to="/payment">
            <button className="proceed-checkout">Proceed to Checkout</button>
          </Link>
        </div>
        {/* <Total/>  */}
      </div>

      <Footer />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  itemCount: selectCartItemsCount,
  total: selectCartTotal,
});
export default connect(mapStateToProps)(ShoppingCart);
