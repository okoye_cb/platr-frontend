import React from "react";
import MainHeader from "../../../components/PagesHeader/MainHeader";
import Navigation from "../../../components/Header/newNavBar";
import TitleAndDescription from "../../../components/PagesHeader/Title";
import platrwhite from "../../../platr.svg"


const AboutHeader = () =>{
    return(
        <div>
            <MainHeader
            renderProp={()=>(
                <div>
                    <Navigation
                    iconColor="icon-pink"
                    idName={"pink-color"}
                    // name={"Menu"}
                    // link={"/"}
                  
                    brand={platrwhite}
                    bgColor="white"/>
                <TitleAndDescription
                title={"About Platr"}/>
            <p className="page-description">These are the restaurants in the selected location, you can choose
                 <br/>another location to show restaurants in another city.</p>
                    </div>
                
            )}/>
        </div>
    )
}




export default AboutHeader;