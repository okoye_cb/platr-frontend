import React from "react";
import "./about.section.scss";


const AboutSection = () =>{
    return(
        <div className="container-fluid">
<div className="conatiner-fluid wrapper">
        <div className="row">

        
          <div className=" col about--section">
            <h3 className="about--section__title" id="title">About Platr</h3>
            <p className="about--section__text" id="text">
            Plater is a Nigerian online and mobile prepared food ordering and delivery marketplace that connects
diners with local takeout restaurants. The online company is based in Kaduna, Nigeria. It was founded in
2019 by Rabiu Oyindamola Rabiu and Ademola Kabir Adigun to create an easier way for diners to get
that food from takeout restaurants. Plater connects diners with a variety of restaurants available near
them. That is, they can order food and get it delivered to their doorsteps.
            </p>            
        </div>
        
        <div className=" col about-section__image">
               <img
            className="img-responsive about-image"
            src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934390/Rectangle_79_raywct.png"
            alt="about_image"
          /> 
            </div>
      </div></div>
        </div>
    )
}


export default AboutSection;