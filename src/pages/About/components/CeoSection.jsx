import React from "react";
import "./ceo.section.scss";
import "bootstrap/dist/css/bootstrap.min.css"


const CeoSection = () => {
  return (
    <div className="container-fluid">
      <div className="row pink-bg">
        <div className="ceo--section col-md-6">
          <p className="ceo--section__team">Our Team</p>
          <h3 className="ceo--section__title">C. E. O</h3>
          <p className="ceo--section__text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem
            ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum
            dolor sit amet,
            <br /> <br />
            <span className="ceo--section__name">Mfonobong Umondia</span>
          </p>
        </div>

        <div className="col-md-6 image--section">
          <img
            className="image--section__vector"
            src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934343/Vector_3_p1dyq2.png"
            alt="vector"
          />
          <img
            className="image--section__photo img-responsive"
            src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934404/Bella-Transparent_bkg_1_rqgetx.png"
            alt="ceo"
          />
        </div>
      </div>

      
    </div>
  );
};

export default CeoSection;
