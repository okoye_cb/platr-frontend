import React from "react";
import "./maincontent.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import AboutSection from "./AboutSection"
import Mission from "./Mission";
import CeoSection from "./CeoSection";
import Counter from './counter';

const MainContent = () => {
  return (
    <div className="main-about-content">
      <AboutSection/>
      <Mission/>
      <CeoSection/>
      <Counter/>
      
    </div>
  );
};

export default MainContent;
