import React from "react";
import "./mission.section.scss";
import "bootstrap/dist/css/bootstrap.min.css"

const Mission = () => {
  return (
    <div className="container-fluid">
      <div class="row">
        <div className="col image-box">
          <img
            src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934347/image_11_ad0q5c.png"
            alt="donut"
            className="macaroni img-responsive"
          />
          <div className="image--one">
            <div className="image--one__transparent"></div>
            <div className="image--one__filled"></div>
            <img
              className="image--one__picture img-responsive"
              src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934359/image_9_vbo6t8.png"
              alt="diary"
            />
          </div>

          <div className="image--two">
            <div className="image--two__transparent"></div>
            <div className="image--two__filled"></div>
            <img
              className="image--two__picture img-responsive"
              src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934341/image_6_n2iowf.png"
              alt="laptop"
            />
          </div>

          <div className="image--three">
            <div className="image--three__transparent"></div>
            <div className="image--three__filled"></div>
            <img
              className="image--three__picture img-responsive"
              src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934340/image_8_n4orp8.png"
              alt="coffee"
            />
          </div>
        </div>

        <div class="col">
          <div class="mission--section">
            <h3 class="mission--section__title">Mission / Vision</h3>
            <p class="mission--section__text">
            Platers mission is to provide our customers with the most professional, fast, dependable and
technologically advanced food delivery service possible. We over here at Plater hope to become the
preferred food ordering and delivering service provider. We hope to accomplish this by providing
customized solutions uniquely designed to meet customer needs and building strong relationships with
individuals and companies that share our core values.{" "}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Mission;
