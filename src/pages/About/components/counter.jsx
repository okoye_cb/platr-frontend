import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendarAlt,
  faUserFriends,
  faCog
} from "@fortawesome/free-solid-svg-icons";
import './counter.scss';
import "bootstrap/dist/css/bootstrap.min.css";

const Counter = () => {
  return (
    <div className="container-fluid counter--wrapper">
      <div className="row counter--section">



        <div className="col founded--section">
          <div className="founded--section__head">
              <div className="founded--section__background"></div>
            <FontAwesomeIcon className="founded--section__icon" icon={faCalendarAlt} />
            <h3 className="founded--section__title">Founded in 2019</h3>
          </div>
          <p className="founded--section__text">
            Lorem ipsum dolor sit amet, consec Lorem ipsum dolor sit amet,
            consec Lorem ipsum dolor
          </p>
        </div>


        <div className="col users--section">
          <div className="users--section__head">
              <div className="users--section__background"></div>
            <FontAwesomeIcon className="users--section__icon" icon={faUserFriends} />
            <h3 className="users--section__title">Users 200,000</h3>
          </div>
          <p className="users--section__text">
            Lorem ipsum dol 200,000 consec Lorem ipsum dolor sit amet, consec
            Lorem ipsum dolor
          </p>
        </div>


        <div className="col office--section">
            <div className="office--section__head">
                <div className="office--section__background"></div>
                <FontAwesomeIcon className="office--section__icon" icon={faCog}/>
                <h3 className="office--section__title">Office 200+</h3>
            </div>
            <p className="office--section__text">Lorem ipsum 200 consec Lorem ipsum dolet, consec Lorem ipsum dolor</p>
        </div>
      </div>



      <div className="row how-to-reach-us">
          <p className="reach-us-text">
          Our headquarters are located at Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/>
Website - http://www.platr.com<br/>
All enquiries, whether made through Email or Phone, would be responded to not later than 48 hours from time of receipt.
          </p>
          <img src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580934349/image_10_eemzmk.png" className="bottom-image" alt="donut"/>
      </div>


    </div>
  );
};

export default Counter;
