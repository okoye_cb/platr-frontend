import React from "react";
import AboutHeader from "./components/Header";
import Footer from "../../components/Footer/index"
import MainContent from "./components/Maincontent"
import './index.style.scss';

const About = () =>{
    return(
        <div>
            <AboutHeader/>
<MainContent/>
<div className="position">
     <Footer/>
</div>
           
        </div>
    )
}


export default About;