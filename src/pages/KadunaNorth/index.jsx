import React, { useEffect } from "react";
import { connect } from "react-redux";
import { getRestaurantActionNorth } from "../../redux/restuarants/restaurantActions";
import "bootstrap/dist/css/bootstrap.css";
import "./restaurantpage.scss";
import RestaurantListCard from "../../components/RestaurantList";
import SidebBar from "./Components/Sidebar";
import Header from "./Components/Header";
import Footer from "../../components/Footer/index";

const KadunaSouth = ({
  getRestaurantActionNorth,
  resloader,
  getRestaurant,
}) => {
  useEffect(() => {
    getRestaurantActionNorth();
  }, [getRestaurantActionNorth]);

  return (
    <div>
      <Header />

      <div className="container ">
        <div className="row">
          <div className="col-md-4">
            <SidebBar />
          </div>
          <div className="col-md-8 restaurant-list-wrapper">
            {resloader && <div className="loader"></div>}
            {getRestaurant
              .filter((restaurant) => {
                return restaurant.status === "active";
              })
              .map((restaurant, index) => (
                <RestaurantListCard
                  key={index}
                  imageUrl={
                    "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/cook-off_md9zlb.png"
                  }
                  RestaurantName={restaurant.businessName}
                  RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
                  PrepTime="50 minutes preparation time"
                  DeliveryTime="20 minutes delivery time"
                  price="#1,500"
                />
              ))}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

const mapStateToProps = (state) => {
  const { resloader, getRestaurant } = state.resturantReducer;
  return {
    resloader,
    getRestaurant,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getRestaurantActionNorth: () => dispatch(getRestaurantActionNorth()),
});

export default connect(mapStateToProps, mapDispatchToProps)(KadunaSouth);
