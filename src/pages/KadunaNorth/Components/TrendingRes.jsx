import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import ReactStarRating from "react-star-ratings-component";
import "./trendingres.scss";

const Trending = (props) => {
  return (
    <div class="card mb-3 shadow-sm trend--card" style={{ maxWidth: "540px" }}>
      <div class="row no-gutters">
        <div class="col-md-4">
          <img
            src={props.imgUrl}
            class="img-responsive trend--card__image"
            alt="..."
          />
        </div>
        <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title trend--card__title">{props.title}</h5>
            <p class="card-text trend--card__text">{props.details}</p>
            <div className="trend--card__star">
              <ReactStarRating
                numberOfStar={5}
                numberOfSelectedStar={5}
                colorEmptyStar="#cfcfcf"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Trending;
