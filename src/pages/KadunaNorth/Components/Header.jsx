import React from "react";
import MainHeader from "../../../components/PagesHeader/MainHeader";
import Navigation from "../../../components/Header/newNavBar";
import TitleAndDescription from "../../../components/PagesHeader/Title";
import Selector from "../../../components/PagesHeader/Selector";
import SubMenu from "./SubMenu";
import SearchBox from "./SearchBox";
import platrwhite from "../../../platr.svg";
const Header = () => {
  return (
    <div>
      <MainHeader
        renderProp={() => (
          <div>
            <Navigation
              idName="pink-color"
              brand={platrwhite}
              iconColor="icon-pink"
              bgColor="white"
            />
            <TitleAndDescription title="Restaurants in Kaduna North" />
            <p className="page-description">
              These are the restaurants in the selected location, you can choose{" "}
              another location to show restaurants in another city.
            </p>
            <Selector />
          </div>
        )}
      />
      <SubMenu />
      <SearchBox />
    </div>
  );
};

export default Header;
