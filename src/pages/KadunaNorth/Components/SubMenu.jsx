import React from "react";
import "./submenu.scss";
import { Link } from "react-router-dom";

const SubMenu = () => {
  return (
    <div className="sub--menu ">
      <div className="container sub--menu__wrap">
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#home">
            Select all
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#news">
            Pasta
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#contact">
            Smoothies
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#about">
            Soups
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#support">
            Drinks
          </Link>
        </div >
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#tools">
            Pizza
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#base">
            Ice Cream
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#custom">
            Rice
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#more">
            Salad
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#logo">
            Pastries
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#friends">
            Chips
          </Link>
        </div>
        <div className="sub--menu__link">
          <Link id="sub--menu__link" to="#partners">
            Barbeque
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SubMenu;
