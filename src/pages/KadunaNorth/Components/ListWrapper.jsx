import React, { useEffect } from "react";
import RestaurantListCard from "../../../components/RestaurantList/index";
import "bootstrap/dist/css/bootstrap.css";

const ListWrapper = () => {
  useEffect(() => {
    console.log("I am mounting now!");
  });

  return (
    <div className="container">
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/cook-off_md9zlb.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/star_chef_yao8xc.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/cold-stone_bomw5h.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/rib_shack_nwwf05.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057947/dominos_sbsawg.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/Shrimp_uejjtn.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057948/grill_house_iv92dn.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/cook-off_md9zlb.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/star_chef_yao8xc.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
      <RestaurantListCard
        imageUrl={
          "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580057949/cold-stone_bomw5h.png"
        }
        RestaurantName="Family Cook Off Restaurant"
        RestaurantDishes=" Snacks, Yoruba Dishes, Continental Dishes, Ice Cream, Drinks"
        PrepTime="50 minutes preparation time"
        DeliveryTime="20 minutes delivery time"
        price="#1,500"
      />
    </div>
  );
};

export default ListWrapper;
