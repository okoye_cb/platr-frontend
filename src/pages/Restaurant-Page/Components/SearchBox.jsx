import React from "react";
import "./searchbox.scss";
import "bootstrap/dist/css/bootstrap.css";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SearchBox = () => {
  return (
    <div className="search-box container">
      <div className="text">
        <p className="hide-on-mobile">50 restaurants</p>
      </div>
      <div className="search-section">
        <div className="search">
          <FontAwesomeIcon
            icon={faSearch}
            color="grey"
            className="search-icon"
          />
          <input
            type="search"
            placeholder="Search restaurant"
            className="input-search"
          />
        </div>
        {/* <div>
          <p className="sort hide-on-mobile">Sort</p>
        </div>
        <div>
          <select className="select-res">
       
              <option value="volvo">Recommended</option>
              <option value="saab">Saab</option>
            
          </select>
        </div> */}
      </div>
    </div>
  );
};
export default SearchBox;
