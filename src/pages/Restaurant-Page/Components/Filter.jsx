import React from "react";
import "./filter.style.scss";
import { Link } from "react-router-dom";

const Filter = () => {
  return (
    <div className="filter--comp">
      <p className="filter--comp__title">Filter with Options</p>
      <div className="wrap">
        <div className="filter--comp__delivery">
         
          <Link to="">To Deliver</Link>
        </div>
        <div className="filter--comp__away">
          
          <Link href="">Take Away</Link>
        </div>
      </div>
    </div>
  );
};

export default Filter;
