import React from "react";
import ReactStarRating from "react-star-ratings-component";
import "./review.style.scss";

const Review = () => {
  return (
    <div className="side--bar">
      <p className="side--bar__review">Reviews</p>
      <ReactStarRating
        numberOfStar={5}
        numberOfSelectedStar={0}
        colorEmptyStar="#cfcfcf"
      />
    </div>
  );
};
export default Review;
