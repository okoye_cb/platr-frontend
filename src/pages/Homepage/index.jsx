import React, { useState } from "react";
import LandingWrapper from "../../components/Header/LandingWrapper";
import { Link } from "react-router-dom";
import Footer from "../../components/Footer";
// import Responsive from "../../components/Slider";
import CustomButton from "../../components/Button";
import "./homepage.styles.scss";
import {restuarantSignUp} from '../../redux/restuarants/signup.actions'
import { connect } from 'react-redux';


const LandingPage = (props) => {
  
  
  const [ restaurant, getResturantInfo ] = useState({
      phone: '',
      businessName: '',
      email: '',
      password: ''
  });

  const updateField = (e) => {
    getResturantInfo({
      ...restaurant,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.restuarantSignUp(restaurant)
  }
  
  const printValues = (e) => {
    e.preventDefault();
    
  };
    return (

      <React.Fragment>
        <LandingWrapper />
        <main className="page-container">
          <section className="about-wrapper">
            <div className="container">
              <div className="row">
                <div className="col-lg-6 col-md 6 col-sm-12">
                  <div className="about-platr">
                    <h2 className="h2">About Platr</h2>
                    <p className="p">
                      Plater is a Nigerian online and mobile prepared food ordering
                      and delivery marketplace that connects diners with local
                      takeout restaurants. The online company is based in Kaduna,
                      Nigeria. It was founded in 2019 by Rabiu Oyindamola Rabiu and
                      Ademola Kabir Adigun to create an easier way for diners to get
                      that food from takeout restaurants. Plater connects diners
                      with a variety of restaurants available near them. That is,
                      they can order food and get it delivered to their doorsteps.
                    </p>
                    {/* <Link to="/about" className="learn-more">
                      Learn more
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580078372/landingPage/arrow_foplsd.png"
                        alt="arrow"
                        className="arrow"
                      />
                    </Link> */}
                  </div>
                </div>
                <div className="col-lg-6 col-md 6 col-sm-12">
                  <div className="box-wrapper">
                    <div className="boxes box-1">
                      <div className="image">
                        <img
                          src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580066853/landingPage/Rectangle_3_cecfbn.png"
                          alt=""
                          className="img"
                        />
                      </div>
                      <div className="image">
                        <img
                          src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580066855/landingPage/Rectangle_3_1_hdyje9.png"
                          alt=""
                          className="img"
                        />
                      </div>
                    </div>
                    <div className="boxes box-2">
                      <div className="image">
                        <img
                          src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580066855/landingPage/Rectangle_3_2_wpwigx.png"
                          alt=""
                          className="img"
                        />
                      </div>
                      <div className="image">
                        <img
                          src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580066855/landingPage/Rectangle_3_3_e9ab3o.png"
                          alt=""
                          className="img"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="how-it-works">
            <div className="container-fluid">
              <h2 className="h2">How it works</h2>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12 content--1">
                  <div className="how__contents ">
                    <div className="icon">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580115357/landingPage/Group_2_minbqy.png"
                        alt="icon"
                      />
                    </div>
                    <h3 className="heading">Select Restaurant</h3>
                    <p className="text">
                      Browse Restaurants <br /> that deliver near you
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 col-md 12 col-sm-12 content--2">
                  <div className="how__contents ">
                    <div className="icon">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580067228/landingPage/Group_grjdsk.png"
                        alt="icon"
                      />
                    </div>
                    <h3 className="heading">Choose your meal</h3>
                    <p className="text">
                      Choose your favorite <br /> meal.
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12 content--3">
                  <div className="how__contents ">
                    <div className="icon">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580067229/landingPage/Group_1_mhyjvj.png"
                        alt="icon"
                      />
                    </div>
                    <h3 className="heading">
                      Receive it at your <br /> doorstep
                    </h3>
                    <p className="text">
                      Your order will be delivered to you in no time
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12 content--4">
                  <div className="how__contents ">
                    <div className="icon">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580067229/landingPage/Group_2_1_obzzkv.png"
                        alt="icon"
                      />
                    </div>
                    <h3 className="heading">Enjoy your order</h3>
                    <p className="text">Enjoy your food item</p>
                  </div>
                </div>
              </div>
    
              <img
                src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580115367/landingPage/Vector_4_xhl9nb.png"
                alt=""
                className="vector-1"
              />
              <img
                src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580115368/landingPage/Vector_5_hzphrw.png"
                alt=""
                className="vector-2"
              />
              <img
                src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580115368/landingPage/Vector_6_lckie8.png"
                alt=""
                className="vector-3"
              />
            </div>
          </section>
    
          <section className="what-we-do">
            <div className="fork-img">
              <img
                src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580067404/landingPage/image_13_g2nlys.png"
                alt=""
              />
            </div>
            <div className="container">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <p className="we-do-text">
                    What we do to help customers eat healthy and atheir convenience.
                  </p>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12  we-do-wrap1">
                  <div className="we-do-wrap">
                    <div className="img">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580067402/landingPage/3_ggndvt.png"
                        alt="img"
                      />
                    </div>
                    <h4 className="wrap-h4">
                      Select your choice meals at the comfort of your home
                    </h4>
                    <p className="we-do-para">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                      do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <Link to="/" className="learn-more">
                      Learn more
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580078372/landingPage/arrow_foplsd.png"
                        alt="arrow"
                        className="arrow"
                      />
                    </Link>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-12 we-do-wrap2">
                  <div className="we-do-wrap">
                    <div className="img">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580067229/landingPage/1_tdwlil.png"
                        alt="img"
                      />
                    </div>
                    <h4 className="wrap-h4">
                      Find Restaurants that are near you to buy from
                    </h4>
                    <p className="we-do-para">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                      do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <Link to="/" className="learn-more">
                      Learn more
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580078372/landingPage/arrow_foplsd.png"
                        alt="arrow"
                        className="arrow"
                      />
                    </Link>
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12 we-do-wrap3">
                  <div className="we-do-wrap">
                    <div className="img">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580067402/landingPage/2_20_azqy9j.png"
                        alt="img"
                      />
                    </div>
                    <h4 className="wrap-h4">
                      Get your food delivered to you at your door step
                    </h4>
                    <p className="we-do-para">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                      do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <Link to="/" className="learn-more">
                      Learn more
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580078372/landingPage/arrow_foplsd.png"
                        alt="arrow"
                        className="arrow"
                      />
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
    
          <section className="register-your-restaurant">
            <div className="container">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-12 right-text">
                  <p className="reg-p">Register your Restaurant</p>
                  <h2 className="reg-h2">Interested in selling on our platform?</h2>
                  <h4 className="reg-h4">
                    Fill out the short form and we will be in touch.
                  </h4>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <div className="form-wrap">
                    <form onSubmit={printValues} className="form-container">
                      <input
                        type="text"
                        className="input-box"
                        placeholder="phone Number"
                        required
                        value={restaurant.phone}
                        name='phone'
                        onChange={updateField}
                      />
                      <input
                        type="text"
                        className="input-box"
                        placeholder="Business Name"
                        required
                        value={restaurant.businessName}
                        name="businessName"
                        onChange={updateField}
                      />
                      <input
                        type="email"
                        className="input-box"
                        placeholder="Email Address"
                        required
                        value={restaurant.email}
                        name='email'
                        onChange={updateField}
                      />
                      <input
                        type="password"
                        className="input-box"
                        placeholder="Password"
                        required
                        value={restaurant.password}
                        name='password'
                        onChange={updateField}
                      />
            { !props.restuarant.ressignuploader ?
            <CustomButton text="Create Account" className="lg" 
            disabled={!restaurant.password}
            onClick={handleSubmit} />
         :
         <CustomButton text="Create Account" className="lg" 
            disabled={!restaurant.password}
            onClick={handleSubmit} />
            // <CustomButton text="" className="lg"  />
            
          }
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
    
          {/* <section className="blog-post">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-sm-12 blog-texts ">
                  <h2 className="blog-h2 text-center">Blog Posts</h2>
                  <p className="blog-p text-center">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. <br />{" "}
                    Voluptatum neque ea iste repellat distinctio dolorum modi, quia
                    ratione
                  </p>
                  <Link to="/" className="read-more">
                    Read more
                    <img
                      src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1580078372/landingPage/arrow_foplsd.png"
                      alt="arrow"
                      className="aarrow"
                    />
                  </Link>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-sm-12" style={{ overFlow: "hidden" }}>
                  <Responsive />
                </div>
              </div>
            </div>
          </section> */}
        </main>
        <Footer />
      </React.Fragment>
    )
  }
  
  const mapStateToProps = state => ({
    restuarant: state.resturantReducer
  })

export default connect(mapStateToProps, { restuarantSignUp })(LandingPage);