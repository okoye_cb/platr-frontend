import React from "react";

import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "bootstrap/dist/css/bootstrap.min.css";

import {
  faFacebookSquare,
  faGooglePlusG
} from "@fortawesome/free-brands-svg-icons";
import "./signup.scss";


const Signup = () => {
  return (
    <div className="signup">
      <h3>Sign up </h3>
      <div className="border">
        <div className="buttons">
          <span className="in"><Link to='/footer'>Sign in</Link></span>
          <span className="up">Sign up</span>
        </div>
        <div className="create">
          <p className="acc">Create Account</p>
          <p className="text">Lorem ipsum dolor sit amet, consectetur </p>
          <p className="tex">adipiscing elit, sed do</p>
        </div>
        <section className="auth">
          <button>
            <FontAwesomeIcon
              icon={faGooglePlusG}
              size="lg"
              color='red'
            />{" "}
            &nbsp; &nbsp;
            <span>Sign up with Google</span>
          </button>
          <button>
            <FontAwesomeIcon
              icon={faFacebookSquare}
              size="lg"
              color='#3B5998'
            />
            &nbsp; &nbsp;
            <span>Sign up with Facebook</span>
          </button>
        </section>
        <section className="form py-4">
          <form>
            <div className='form-desktop'>
                <div className="form-row">
                  <div class="col">
                    <label for="inputEmail4">First Name</label>
                    <input
                      type="text"
                      class="form-control"
                      placeholder="First name"
                    />
                  </div>
                  <div class="col">
                    <label for="inputEmail4">Last Name</label>
                    <input
                      type="text"
                      class="form-control"
                      placeholder="Last name"
                    />
                  </div>
                </div>
                <div className="form-row py-4">
                  <div class="col">
                    <label for="inputEmail4">Email Address</label>
                    <input
                      type="text"
                      class="form-control"
                      placeholder="Email Address"
                    />
                  </div>
                  <div class="col">
                    <label for="inputEmail4">Phone Number</label>
                    <input
                      type="number"
                      class="form-control"
                      placeholder="Phone Number"
                    />
                  </div>
                </div>
                <div className="form-row">
              <div class="col">
                <label for="inputEmail4">House Address</label>
                <input
                  type="text"
                  class="form-control"
                  placeholder="House Address"
                />
              </div>
              <div class="col">
                <label for="inputEmail4">Password</label>
                <input
                  type="password"
                  class="form-control"
                  placeholder="Password"
                />
              </div>
            </div>
            </div>
            <div  className='form-mob'>
              <div className='form-group'>
                <div className='py-3'>
                  <label for="formGroupExampleInput" >First Name</label>
                  <input type="text" className="form-control" id="formGroupExampleInput" placeholder="First Name" />
                </div>
                <div className='py-3'>
                <label for="formGroupExampleInput" >Last Name</label>
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Last Name" />
                </div>
                <div className='py-3'>
                  <label for="formGroupExampleInput" >Email Adddres</label>
                  <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Email Adddres" />
                </div>
                <div className='py-3'>
                  <label for="formGroupExampleInput" >Phone Number</label>
                  <input type="number" className="form-control" id="formGroupExampleInput" placeholder="Phone Number" />
                </div>
                <div className='py-3'>
                <label for="formGroupExampleInput" >House Address</label>
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="House Address" />
                </div>
                <div>
                  <label for="formGroupExampleInput" >Password</label>
                  <input type="password" className="form-control" id="formGroupExampleInput" placeholder="Password" />
                </div>
              </div>
            </div>
            <h6 className="py-3">
              By clicking Sign in, you agree to our Terms of Use and Privacy
              Policy
            </h6>

            <button className="signin">Sign In</button>
          </form>

          <div className="retrieve">
            <Link to="/forgotpassword" className="forgot">
              forgot your password?
            </Link>
          </div>
        </section>
      </div>
    </div>
  );
};

export default Signup;
