import React, { Component } from "react";
import platr from "../../platr.svg";

import "bootstrap/dist/css/bootstrap.min.css";
import Navigation from "../../components/Header/newNavBar";

import "./Authpage.scss";
import SignIn from "./Forms/signin"
import SignUp from "./Forms/signup"
import { connect } from 'react-redux'
import  { Loading } from '../../redux/user/signup.action'
import { signUpWithEmailAndPassword } from '../../redux/user/signup.action'
import { signInWithEmailAndPassword } from '../../redux/user/signup.action'
class AuthPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signin: true,
      signup: false,
    };
  }

  handleSignin() {
    this.setState({ signin: !this.state.signup, signup: false });
  }

  handleSignup() {
    this.setState({ signup: !this.state.signin, signin: false });
  }

  render() {
    const bg = this.state.signin ? "bgc" : "nbgc";
    const cg = !this.state.signin ? "bgc" : "nbgc";
    const classes = `in ${bg}`;
    const classo = `up ${cg}`;
    return (
      <div className="access ">
        <div style={{ background: "", width: "100vw",}}>
          <div>
            <Navigation
              idName="pink-color"
              brand={platr}
              bgColor="white"
              iconColor="icon-pink"
            />
          </div>
          <div className="bordered">
            <div className="buttons">
              <span className={classes} onClick={() => this.handleSignin()}>
                Sign in
              </span>
              <span className={classo} onClick={() => this.handleSignup()}>
                Sign up
              </span>
            </div>
            {this.state.signin ? (
              <SignIn handleSignin={this.handleSignin} {...this.props} />
            ) : (
              <SignUp handleSignup={this.handleSignup} {...this.props}/>
            )}
          </div>
         
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.userReducer,
});

export default connect(mapStateToProps, {Loading, signUpWithEmailAndPassword, signInWithEmailAndPassword })( AuthPage)
