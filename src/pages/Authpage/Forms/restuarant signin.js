import React, { useState } from "react";
import '../Authpage.scss'
import { Link } from "react-router-dom";
import Navigation from "../../../components/Header/newNavBar";
import platr from "../../../platr.svg";
import restuarantSignIn from '../../../redux/restuarants/signup.actions'
import { connect } from 'react-redux'
import '../Authpage.scss'

const RestuarantSignIn = (props) => {
  
  const [ resturant, getresturantInfo ] = useState({
    email: '',
    password: ''
  });

  const updateField = (e) => {
    getresturantInfo({
      ...resturant,
      [e.target.name] : e.target.value
    })
  }

    const handleSignIn = (e) => {
    
      e.preventDefault()
      props.restuarantSignIn(resturant)
    }

    const printValues = (e) => {
      e.preventDefault();
      
    };
  


     

  return (
      <div className='access'>
        <div style={{ background: "", width: "100vw"}}>
  {/* <div></div> */}
    <Navigation
      idName="pink-color"
      brand={platr}
      bgColor="white"
      iconColor="icon-pink"
    />
  </div>
  <div className='bordered'>
        <div className="create">
          <p className="acc">Sign in into Platr</p>
          <p className="text">Lorem ipsum dolor sit amet, consectetur </p>
          <p className="tex">adipiscing elit, sed do</p>
        </div>
        
        <section className="form py-4">
          <form onSubmit={printValues}>
            <div className="form-mob-desktop-signin">
              <div className="form-row ">
                <label for="inputEmail4">Email</label>
                <input
                  type="email"
                  class="form-control h "
                  placeholder=" Restuarant Email Address"
                  required='required'
                  name='email'
                  value={resturant.email}
                  onChange={updateField}
                />
              </div>
              <div className="form-row py-4">
                <label for="inputEmail4">Password</label>
                <input
                  type="password"
                  class="form-control h"
                  placeholder="Password"
                  required='required'
                  name='password'
                  value={resturant.password}
                  onChange={updateField}
                />
              </div>
            </div>
            <h6 className="py-3 sign">
              By clicking Sign in, you agree to our Terms of Use and Privacy
              Policy
            </h6>
            {props.restuarant.resloader ?
              <div className='overlay'>
                <div className='loader'></div>
              </div>

              : ''}
            <button className="signin" 
             onClick={ (e) => handleSignIn(e) }
            //  disabled
             disabled= { !resturant.password || !resturant.email}
            >Sign In
            </button>
          </form>

          <div className="retrieve">
            <Link to="/forgotpassword" className="forgot">
              forgot your password?
            </Link>
          </div>
        </section>
      </div>
      </div>

  )
}

const mapStateToProps = state => ({
   restuarant: state.resturantReducer

})

export default connect(mapStateToProps, { restuarantSignIn }) (RestuarantSignIn);
