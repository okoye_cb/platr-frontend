import React, { useState, useCallback } from "react";
import { withRouter } from "react-router-dom";
import {
  googleProvider,
  signInWithGoogle,
  auth, signInWithFacebook, facebookProvider
} from "../../../firebase/firebaseConfig";
import {
  faFacebookSquare,
  faGooglePlusG,
} from "@fortawesome/free-brands-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
// import { signInWithFacebook } from '../../../firebase/firebase.facebook';


const SignIn = (props) => {

  const [btnState, setBtnState] = useState(false);
  const handleGoogleAuthSignIn = useCallback(
    async (event) => {
      event.preventDefault();
      setBtnState(true);
      try {
        await signInWithGoogle(googleProvider);
        const userName = auth.currentUser.displayName;
        const userId = auth.currentUser.uid;
        const userEmail = auth.currentUser.email;
        const photoUrl = auth.currentUser.photoURL;
        const userPhone = auth.currentUser.phoneNumber;
        const userData = {
          name: userName,
          id: userId,
          email: userEmail,
          photo: photoUrl,
          phone: userPhone
        };
        
        
        window.localStorage.setItem("currentUser", JSON.stringify(userData));
        props.history.push("/");
      } catch (error) {
        console.log(error.message);
      }
    },
    [props.history]
  );

  const handleFacebookAuthSignIn = useCallback(
    async (event) => {
      event.preventDefault();
      setBtnState(true);
      try {
        await signInWithFacebook(facebookProvider);
        const userName = auth.currentUser.displayName;
        const userId = auth.currentUser.uid;
        const userEmail = auth.currentUser.email;
        const photoUrl = auth.currentUser.photoURL;
        const userPhone = auth.currentUser.phoneNumber;

        const userData = {
          name: userName,
          id: userId,
          email: userEmail,
          photo: photoUrl,
          phone: userPhone
        };
        
        window.localStorage.setItem("currentUser", JSON.stringify(userData));
        props.history.push("/");
      } catch (error) {
        console.log(error.message);
      }
    },
    [props.history]
  );

  const [ user, getUserInfo ] = useState({
    email: '',
    password: ''
  });

  const updateField = (e) => {
    getUserInfo({
      ...user,
      [e.target.name] : e.target.value
    })
  }

    const handleSignIn = (e) => {
      e.preventDefault()
      props.signInWithEmailAndPassword(user)
    }

    const printValues = (e) => {
      e.preventDefault();
      
    };
  


     

  return (
      <div>
        <div className="create">
          <p className="acc">Sign in to your Account</p>
          <p className="text">Lorem ipsum dolor sit amet, consectetur </p>
          <p className="tex">adipiscing elit, sed do</p>
        </div>
        <section className="auth">
        <button onClick={handleGoogleAuthSignIn}>
          <FontAwesomeIcon icon={faGooglePlusG} size="lg" color="red" /> &nbsp;
          &nbsp;
          {btnState === true ? (
            <span>Please wait...</span>
          ) : (
            <span>Sign in with Google</span>
          )}
        </button>
        <button onClick={handleFacebookAuthSignIn}>
          <FontAwesomeIcon icon={faFacebookSquare} size="lg" color="#3B5998" />
          &nbsp; &nbsp;
          <span>Sign in with Facebook</span>
        </button>
      </section>
        <section className="form py-4">
          <form onSubmit={printValues}>
            <div className="form-mob-desktop-signin">
              <div className="form-row ">
                <label for="inputEmail4">Email</label>
                <input
                  type="email"
                  class="form-control h "
                  placeholder="Email Address"
                  required='required'
                  name='email'
                  value={user.email}
                  onChange={updateField}
                />
              </div>
              <div className="form-row py-4">
                <label for="inputEmail4">Password</label>
                <input
                  type="password"
                  class="form-control h"
                  placeholder="Password"
                  required='required'
                  name='password'
                  value={user.password}
                  onChange={updateField}
                />
              </div>
            </div>
            <h6 className="py-3 sign">
              By clicking Sign in, you agree to our Terms of Use and Privacy
              Policy
            </h6>
            {props.user.signinloader ?
              <div className='overlay'>
                <div className='loader'></div>
              </div>

              : ''}
            <button className="signin" 
             onClick={ (e) => handleSignIn(e) }
            //  disabled
             disabled= { !user.password || !user.email}
            >Sign In
            </button>
          </form>

          <div className="retrieve">
            <Link to="/forgotpassword" className="forgot">
              forgot your password?
            </Link>
          </div>
        </section>
      </div>

  )
}
  


export default withRouter(SignIn);
