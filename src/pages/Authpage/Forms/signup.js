import React, { useState, useCallback } from "react";
import { withRouter } from "react-router-dom";
import {
  googleProvider,
  signInWithGoogle,
  signInWithFacebook,
  facebookProvider
} from "../../../firebase/firebaseConfig";
import {
  faFacebookSquare,
  faGooglePlusG,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import "../Authpage.scss";

const SignUp = (props) => {
  const [userCred, updateUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    password: "",
    confirmPassword: "",
  });

  const [btnState, setBtnState] = useState(false);
  const handleGoogleAuthSignUp = useCallback(
    async (event) => {
      event.preventDefault();
      setBtnState(true);
      try {
        await signInWithGoogle(googleProvider);
        props.history.push("/");
      } catch (error) {
        console.log(error.message);
      }
    },
    [props.history]
  );

  const handleFacebookAuthSignUp = useCallback(
    async (event) => {
      event.preventDefault();
      setBtnState(true);
      try {
        await signInWithFacebook(facebookProvider);
        props.history.push("/");
      } catch (error) {
        console.log(error.message);
      }
    },
    [props.history]
  );

  const updateField = (e) => {
    updateUser({
      ...userCred,
      [e.target.name]: e.target.value,
    });
  };
  

  const handleSignUp = (event) => {
    event.preventDefault();
    props.signUpWithEmailAndPassword(userCred);
  };

  const printValues = (e) => {
    e.preventDefault();
    
  };

  return (
    <div>
      <div>
        <div className="create">
          <p className="acc">Create Account</p>
          <p className="text">Lorem ipsum dolor sit amet, consectetur </p>
          <p className="tex">adipiscing elit, sed do</p>
        </div>
        <section className="auth">
          <button onClick={handleGoogleAuthSignUp}>
            <FontAwesomeIcon icon={faGooglePlusG} size="lg" color="red" />
            &nbsp; &nbsp;
            {btnState === true ? (
              <span>Please wait...</span>
            ) : (
              <span>Sign up with Google</span>
            )}
          </button>
          <button onClick={handleFacebookAuthSignUp}>
            <FontAwesomeIcon
              icon={faFacebookSquare}
              size="lg"
              color="#3B5998"
            />
            &nbsp; &nbsp;
            <span>Sign up with Facebook</span>
          </button>
        </section>
        <section className="form py-4">
          <form onSubmit={printValues}> 
            <div className="form-desktop">
              <div className="form-row">
                <div class="col">
                  <label for="inputEmail4">First Name</label>
                  <input
                    type="text"
                    class="form-control h"
                    value={userCred.firstName}
                    name="firstName"
                    onChange={updateField}
                    placeholder="First name"
                    required
                  />
                </div>
                <div class="col">
                  <label for="inputEmail4">Last Name</label>
                  <input
                    type="text"
                    class="form-control h"
                    placeholder="Last name"
                    value={userCred.lastName}
                    name="lastName"
                    onChange={updateField}
                    required
                  />
                </div>
              </div>
              <div className="form-row py-4">
                <div class="col">
                  <label for="inputEmail4">Email Address</label>
                  <input
                    type="email"
                    class="form-control h"
                    placeholder="Email Address"
                    value={userCred.email}
                    name="email"
                    onChange={updateField}
                    required
                  />
                </div>
                <div class="col">
                  <label for="inputEmail4">Phone Number</label>
                  <input
                    type="number"
                    class="form-control h"
                    placeholder="Phone Number"
                    value={userCred.phone}
                    name="phone"
                    onChange={updateField}
                    required
                  />
                </div>
              </div>
              <div className="form-row">
                <div class="col">
                  <label for="inputEmail4">Password</label>
                  <input
                    type="password"
                    class="form-control h"
                    placeholder="password"
                    value={userCred.password}
                    name="password"
                    onChange={updateField}
                    required
                  />
                </div>
                <div class="col">
                  <label for="inputEmail4">Confirm Password</label>
                  <input
                    type="password"
                    class="form-control h"
                    placeholder="Password"
                    value={userCred.confirmPassword}
                    name="confirmPassword"
                    onChange={updateField}
                    required
                  />
                </div>
              </div>
            </div>
            <div className="form-mob">
              <div className="form-group">
                <div className="py-3">
                  <label for="formGroupExampleInput">First Name</label>
                  <input
                    type="text"
                    className="form-control h"
                    id="formGroupExampleInput"
                    placeholder="First Name"
                    name="firstName"
                    onChange={updateField}
                    required
                  />
                </div>
                <div className="py-3">
                  <label for="formGroupExampleInput">Last Name</label>
                  <input
                    type="text"
                    className="form-control h"
                    id="formGroupExampleInput"
                    placeholder="Last Name"
                    value={userCred.lastName}
                    name="lastName"
                    onChange={updateField}
                    required
                  />
                </div>
                <div className="py-3">
                  <label for="formGroupExampleInput">Email Adddres</label>
                  <input
                    type="email"
                    className="form-control h"
                    id="formGroupExampleInput"
                    placeholder="Email Adddres"
                    value={userCred.email}
                    name="email"
                    onChange={updateField}
                    required
                  />
                </div>
                <div className="py-3">
                  <label for="formGroupExampleInput">Phone Number</label>
                  <input
                    type="number"
                    className="form-control h"
                    id="formGroupExampleInput"
                    placeholder="Phone Number"
                    value={userCred.phone}
                    name="phone"
                    onChange={updateField}
                    required
                  />
                </div>
                <div className="py-3">
                  <label for="formGroupExampleInput">password</label>
                  <input
                    type="password"
                    className="form-control h"
                    id="formGroupExampleInput"
                    placeholder="password"
                    value={userCred.password}
                    name="password"
                    onChange={updateField}
                    required
                  />
                </div>
                <div>
                  <label for="formGroupExampleInput">Confirm Password</label>
                  <input
                    type="password"
                    className="form-control h"
                    id="formGroupExampleInput"
                    placeholder="Password"
                    value={userCred.confirmPassword}
                    name="confirmPassword"
                    onChange={updateField}
                    required
                  />
                </div>
              </div>
            </div>

            <h6 className="py-3 sign">
              By clicking Sign in, you agree to our Terms of Use and Privacy
              Policy
            </h6>
            {props.user.loader ? (
              <div className="overlay">
                <div className="loader"></div>
              </div>
            ) : (
              ""
            )}

             

            <button
              onClick={(event) => handleSignUp(event)}
              disabled={!userCred.firstName || !userCred.lastName || !userCred.email || !userCred.password || !userCred.confirmPassword || !userCred.phone }
              className="signin"
            > 
              Sign Up</button>
          </form>

          <div className="retrieve">
            <Link to="/forgotpassword" className="forgot">
              forgot your password?
            </Link>
          </div>
        </section>
      </div>
    </div>
  );
};

export default withRouter(SignUp);
