import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLandmark } from "@fortawesome/free-solid-svg-icons";

import Modal from "react-bootstrap/Modal";
import "./modal.scss";

const CustomModal = (props) => {
  return (
    <>
      <div style={{ boxShadow: " 7px 35px 100px rgba(0, 0, 0, 0.1);" }}></div>
      <Modal {...props} centered lg className="custom-modal">
        {/* <Modal.Header >OverView Pepe's Resturant</Modal.Header> */}
        <Modal.Title></Modal.Title>
        <Modal.Body>
          <div>
            <section>
              <div>
                <h4
                  style={{
                    fontSize: "16px",
                    fontWeight: "bold",
                    color: "#1f3f68",
                    textAlign: "center",
                  }}
                >
                  OverView Pepe's Restuarant
                </h4>
              </div>
              <div style={{ fontSize: "12px" }}>
                Base prepared fresh daily. Extra toppings are available in
                choose extra Choose you sauce: Go for BBQ sauce or piri piri
                sauce on your pizza base for no extra cost.Choose your cut:
                Triangular, square, fingers.
              </div>
            </section>
            <section className="fi">
              <div>
                <h4
                  style={{
                    fontSize: "16px",
                    color: "#1f3f68",
                    textAlign: "center",
                  }}
                >
                  Contact Details
                </h4>
                <ul className="list">
                  <li>
                    <FontAwesomeIcon icon={faLandmark} size="sm" color="grey" />
                    <span>Pizza Express Resturant, Boundary Ajegunle</span>
                  </li>
                  {/* <li> 
                        <FontAwesomeIcon icon={faPhone} size='sm' color='grey' />
                        <span><a href='tel: 539535385204857259'>539535385204857259</a></span>
                      </li>
                      <li> 
                        <FontAwesomeIcon icon={faGlobe} size='sm' color='grey' />
                        <span><a href='#'>www.pepesresturaant.com</a></span>
                      </li>
                      <li> 
                        <FontAwesomeIcon icon={faEnvelope} size='sm' color='grey' />
                        <span><a href='mailto: '>send Enquiry By Email </a></span>
                      </li> */}
                </ul>
              </div>
              <div>
                <h4
                  style={{
                    fontSize: "16px",
                    color: "#1f3f68",
                    textAlign: "center",
                  }}
                >
                  Opening hours
                </h4>
                <section>
                  <label for="hrs"></label>
                  <select
                    style={{
                      fontSize: "14px",
                      color: "#1f3f68",
                      background: "none",
                      height: "2em",
                    }}
                  >
                    <option value="mon-fri">Mon - Fri 8am - 9pm</option>
                    <option value="Saturdays">Saturdays 8am - 9pm</option>
                    <option value="Sundays">Sunday closed </option>
                  </select>
                </section>
              </div>
            </section>
          </div>
        </Modal.Body>
        {/* <Modal.Footer></Modal.Footer> */}
      </Modal>
    </>
  );
};

// CustomModal.propTypes = {
//   title: PropTypes.func.isRequired,
//   body: PropTypes.func.isRequired,
//   footer: PropTypes.func
// };

export default CustomModal;
