import React, { useState } from "react";

// import ReactStarRating from "react-star-ratings-component";
import Modal from "./Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExclamationCircle,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";

import "bootstrap/dist/css/bootstrap.min.css";
import "./review.scss";

const Review = () => {
  const [show, toggleModal] = useState(false);
  return (
    <div className="reviews">
      <>
        <Modal show={show} onHide={() => toggleModal(false)} />
      </>
      <div>
        {/* <h4 className="col-md-4">Reviews</h4> */}
        <div className="info">
          {/* <span className="star col-md-4">
            <ReactStarRating
              numberOfStar={5}
              numberOfSelectedStar={4}
              colorEmptyStar="whitesmoke"
              colorFilledStar="#FFBA49"
              spaceBetweenStar="2px"
              starSize="25px"
            />
          </span> */}
          <span className="buttons">
            <button>
              <FontAwesomeIcon
                icon={faExclamationCircle}
                size="lg"
                color="grey"
              />
              <span onClick={() => toggleModal(!show)}>Resturant Info</span>
            </button>
            <button>
              <FontAwesomeIcon icon={faHeart} size="lg" color="grey" />
              <span>Add to Favourite</span>
            </button>
          </span>
        </div>
      </div>
      {/* <div className='available'>
        <span className='col-md-4 d'>
          <span>Open</span> - <span>Closes 9PM</span>
        </span>
        <span className='col-md-3 d'>
          <span>Monday</span> - <span>Friday 8AM - 9PM</span>
        </span>
        <span className='col-md-3 d'>
          <span>Saturday</span> - <span>8AM - 8PM</span>
        </span>
        <span className='col-md-3 d'>
          <span>Sundays</span> - <span>Closed</span>
        </span>
      </div> */}
      {/* <div className='available-mob'>
        <div className='row'>
        <span className='col-md-4' >
          <span>Open</span> - <span>Closes 9PM</span>
        </span>
        <span className='col-md-3'>
          <span>Monday</span> - <span>Friday 8AM - 9PM</span>
        </span>
        <span className='col-md-3'>
          <span>Saturday</span> - <span>8AM - 8PM</span>
        </span>
        <span className='col-md-3'>
          <span>Sundays</span> - <span>Closed</span>
        </span>
        </div>
    </div> */}
    </div>
  );
};

export default Review;
