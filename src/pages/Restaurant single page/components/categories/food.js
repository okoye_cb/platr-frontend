import React from 'react'
import { Link } from 'react-router-dom'
import './food.scss'
const Food = () => {
  return (
    <div className='store'>
      <h4 className='text'>Store Category</h4>
      <ul className='list'>
        <li className='item'>
          <Link to='/soups'>Soups</Link>
        </li>
        <li className='item'>
          <Link to='/junks'>Junk Food</Link>
        </li>
        <li className='item'>
          <Link to='/junks'>Continental Dishes</Link>
        </li>
        <li className='item'>
          <Link to='/junks'>Drinkable</Link>
        </li>
        <li className='item'>
          <Link to='/breakfast'>Breakfast Combos</Link>
        </li>
        <li className='item'>
          <Link to='/launch'>Launch Combos</Link>
        </li>
        <li className='item'>
          <Link to='/to'>Diner Combos</Link>
        </li>
      </ul>
    </div>
  )
}

export default Food;
