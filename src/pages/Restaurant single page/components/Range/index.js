import "rc-slider/assets/index.css";
import "rc-tooltip/assets/bootstrap.css";
import React from "react";
import Slider from "rc-slider";
import Tooltip from "rc-slider";
import "./range.scss";

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const Handle = Slider.Handle;

const RangeSlide = props => {
  const { value, dragging, index, ...restProps } = props;
  const wrapperStyle = { width: 200 };
  return (
    <div id="range--comp">
      <Tooltip
        prefixCls="rc-slider-tooltip"
        overlay={value}
        visible={dragging}
        placement="left"
        key={index}
      >
        <Handle value={value} {...restProps} />
      </Tooltip>
      <div>
        (70,0000) found
      </div>
      <div id="range" style={wrapperStyle}>
        <Range
          min={0}
          max={1000}
          defaultValue={[0, 1000]}
          tipFormatter={value => `N ${value} . 00`}
          trackStyle={[{ backgroundColor: "#EB2188", borderRadius: "5rem" }]}
          handleStyle={[
            {
              backgroundColor: "#F2F2F2",
              border: "1px solid #BDBDBD",
              height: "1rem",
              width: "1rem",
              boxSizing: "border-box"
            },
            {
              backgroundColor: "#F2F2F2",
              border: "1px solid #BDBDBD",
              height: "1rem",
              width: "1rem",
              boxSizing: "border-box"
            }
          ]}
          railStyle={{ backgroundColor: "#F2BBD7" }}
        />
      </div>
      {/* <div>
        <button id="range--comp__button">Refine Search</button>
      </div>*/}
    </div> 
  );
};

export default RangeSlide;
