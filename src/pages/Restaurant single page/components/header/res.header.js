import React from "react";

import MainHeader from "../../../../components/PagesHeader/MainHeader";
import Navigation from "../../../../components/Header/newNavBar";
import platrwhite from "../../../../platr.svg";
import TitleAndDescription from "../../../../components/PagesHeader/Title";
import "./header.scss";

const Header = () => (
  <div className="header">
    <div>
      <MainHeader
        className="bg"
        renderProp={() => (
          <div>
            <Navigation
              idName="pink-color"
              // name="Menus"
              cartTitleOrBlog="Cart"
              cartOrBlogLink ={"/cart"}
              brand={platrwhite}
              iconColor="icon-pink"
              bgColor="white"
              
             
            />
            <div className="title">
              <TitleAndDescription
                className="title"
                title={"PePe's Restuarant"}
              />
            </div>
          </div>
        )}
      />
    </div>
  </div>
);

export default Header;
