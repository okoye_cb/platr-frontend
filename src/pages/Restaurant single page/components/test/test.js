import React from "react";
import Cards from "../soup cards/Cards";
import Review from "../reviewbar/review";
import Filter from "../filter/filter";
import Food from "../FoodsCard";

import "./test.scss";
function Test() {
  return (
    <div className="test">
      <section className="r">
        <Review />
      </section>
      <div className="cf">
        <section className="c">
          <Cards />
        </section>
        <div className="f">
          <Filter />
          <Food />
        </div>
      </div>
    </div>
  );
}

export default Test;
