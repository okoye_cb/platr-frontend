import React from 'react'
import "bootstrap/dist/css/bootstrap.css";

import './card.scss'

const Card = (props) => {
  const { imgSrc, Soup, price, kitchen} = props
  return (
    <div className='card shadow-md cards' style={{width: "335px", height:'120px'}}>
      <div className='  '>

      <div className='row'>
        <div className='col-md-4'>
          <img src={`${imgSrc}`} alt='delicious soups' style={{ height:'120px' }} />
        </div>

        <div className='col-md-8 ' >
          <div className='card-body co' >
            <h3 className='foodName'>{Soup}</h3>
            <h5 className='price'>&#x20A6; {` ${price}`}</h5>
            <h5 className='kitchen'>{kitchen}</h5>
          </div>
        </div>
      </div>
      </div>

    </div>
  )
}

export default Card
