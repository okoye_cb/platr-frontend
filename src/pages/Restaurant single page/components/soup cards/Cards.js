import React, { Component } from 'react'

import Card from './card-item/card-item'
import Food from '../categories/food'
import Hotsoup from './mockdata';


import './cards.scss'
class Cards extends Component {
  state = {
    mockdata: Hotsoup,
  }
  
  render() {
    
    return (
      <div className='a'>  
          <Food />
        <div className='cards' style={{marginLeft: '15px', marginTop:'3rem'}}>
          <div  className='top' >
            <h4 className='top-sales'>Top Selling</h4>
          </div>
          { 
            this.state.mockdata.map(soup => <Card key={soup.id} {...soup} />)        
          }
        </div>
          
      </div>
    )
  }
}

export default Cards