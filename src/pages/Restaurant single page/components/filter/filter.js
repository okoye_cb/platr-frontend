import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGripHorizontal, faSearch } from "@fortawesome/free-solid-svg-icons";
import "bootstrap/dist/css/bootstrap.min.css";
import "./filter.scss";
import RangeSlide from "../Range";

const Filter = () => {
  return (
    <>
      <div className="filter">
        <div className="divOne">
          <section className="search">
            <h3 className="fs">Search store &gt;</h3>
            <section className="input-icon">
              <section className="icon">
                <FontAwesomeIcon icon={faSearch} color="#1F3F68" size="sm" />
              </section>
              <input className="input" type="search" placeholder="search" />
            </section>
          </section>
          <section className="divTwo">
            <section className="sec">
              <section className="col-md-3 desk">
                <FontAwesomeIcon icon={faSearch} color="#1F3F68" size="sm" />
                <span style={{ paddingLeft: "5px" }}>Filter</span>
              </section>
              <section className="col-md-3 mob">
                <FontAwesomeIcon icon={faSearch} color="#1F3F68" size="sm" />
              </section>

              <section className="col-md-7 desk">
                <FontAwesomeIcon icon={faSearch} color="#1F3F68" size="sm" />
                <span style={{ paddingLeft: "5px" }}>
                  price: lowest - highest
                </span>
              </section>
              <section className="col-md-3 mob">
                <FontAwesomeIcon icon={faSearch} color="#1F3F68" size="sm" />
              </section>
              <section className="col-md-2">
                <FontAwesomeIcon
                  icon={faGripHorizontal}
                  color="#1F3F68"
                  size="sm"
                />
              </section>
            </section>
          </section>
        </div>
      </div>
      <div>
        <RangeSlide />
      </div>
    </>
  );
};

export default Filter;
