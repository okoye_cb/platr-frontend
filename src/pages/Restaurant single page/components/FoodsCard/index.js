import React, { useState } from "react";

import Card from "./card";
import Menu from "../FoodsCard/mockdata1";

import "./foodcard.scss";

const Food = () => {
  const [foodItem] = useState(Menu);
  

  return (
    <div className="e">
      {/* <div className="foodcard" style={{ width: "95%", margin: "1rem auto" }}>
        {foodItem.map((item) => (
          <Card key={item.id} {...item} />
        ))}
      </div> */}
      <div className="container">
        <div className="row">
          {foodItem.map((item) => (
            <div className="col-lg-3 col-sm-12 foodcard">
              <Card key={item.id} {...item} />
            </div>
          ))}
        </div>
      </div>
      <div className="pagn">
        <span className="lr">&lt;</span>
        <span className="pg"> page 2 of 3 </span>
        <span className="rr"> &gt;</span>
      </div>
    </div>
  );
};

export default Food;
