import React, {useState} from 'react'
import Modal from './orderModal/Modal'

import './card.scss'

const Card = (item) => {
  const {menu, imgSrc, price, quantity } = item
  const [show, setShow] = useState(false)
  return (
    <div>
      <>
        <Modal show={show} item={item} onHide={() => setShow(false)} />
      </>
      <div onClick={() => setShow(true)} className="card shadow-md fc">
        {/* <div className="row a"> */}
        <div className="b">
          <img src={`${imgSrc}`} alt="cardimage" className="card-images" />
        </div>
        <div className="">
          <div className="card-body">
            <h3 className="food">{menu}</h3>
  <h4 className="price"> &#x20A6;{ quantity} *  {price}</h4>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Card
