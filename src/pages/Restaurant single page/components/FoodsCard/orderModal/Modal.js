import React from "react";
import { connect } from 'react-redux';
import { addItem } from '../../../../../redux/cartReducer/cart-actions'

import Modal from "react-bootstrap/Modal";
import './modal.scss'

const CustomModal = (props) => {
  const {imgSrc, menu, price, desc, time} = props.item;
  const { item, addItem } = props;

  const addToCart = (e) =>{
    e.preventDefault();
    addItem(item);
  
  };

 
  
  return (
    <>
    <div style={{boxShadow: ' 7px 35px 100px rgba(0, 0, 0, 0.1);'}}>
      <Modal {...props}  
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton></Modal.Header>
        <Modal.Title >
        </Modal.Title>
        <Modal.Body>
          <div className='body' >
            <div className='sz'> 
            <section className='imgsec' >
              <img class='foodi' src={`${imgSrc}`} alt='food'/>
            </section>
          <div >
            <h4 className='menu'>{`${menu}`}</h4>
            <h5 className='mdet'>{desc}</h5>
            
            <h5 className='delivery'>{time}</h5>

            <div className='detail'>
              <h4 className='price'>{`Price: ${price}`}</h4>
              {/*<h4 className='qty'>Quantity: 
               <span style={{marginLeft:'15px', cursor: 'pointer'}} onClick={() => qtyAdd(item)}> + </span> 
              <span style={{border:'1px solid grey', padding:'5px', margin:'0 5px', borderRadius:'5px'}}> {quantity} </span> 
              <span style={{cursor:'pointer', marginLeft:'5px'}} onClick={() => qtyRemove(item)}> - </span> </h4> */}
              <h4 className='del'>Delivery Detail:  N100</h4>
            </div>
          </div>

            </div>
            <div className='btnz'>
              <span>
                <button className='btn1' onClick={addToCart}>
                  Add To Cart
                </button>
              </span>
              <span>
                <button className='btn2'>
                  Buy Now
                </button>
             
              </span>
            </div>
          </div>
        </Modal.Body>
        
      </Modal>
    </div>

    </>
  );
};


const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch(addItem(item)),
 
})

export default connect(null, mapDispatchToProps)(CustomModal);
