import React from "react";

import MainHeader from "../../components/PagesHeader/MainHeader";
import Navigation from "../../components/Header/newNavBar";
import TitleAndDescription from "../../components/PagesHeader/Title";
import CustomButton from "../../components/Button";
import Footer from "../../components/Footer";
import platrwhite from "../../platr.svg"

import "bootstrap/dist/css/bootstrap.css";

import "./contact.scss";

const Contact = () => (
  <div className="contact">
    <MainHeader
      renderProp={() => (
        <div>
          <Navigation
            idName={"pink-color"}
            brand={platrwhite}
            bgColor={'white'}
            iconColor={'icon-pink'}
            
          />
          <TitleAndDescription title={"Contact Us"} />
          <div className='center'>
        <p>  Provide a valid, navigable address as the href value. If you cannot provide a valid href, </p> 
          </div>
          {/* <h4>This is a big header</h4> */}
        </div>
      )}
    />
    <div className="body">
      <section className="address">
        <p>Our headQuaters is located at - some address in kaduna </p>

        <div className="enq">
          <p>
            For Complaints - <a href="tel: 439558324823043">439558324823043</a>
          </p>
          <p>
            For Enquiries -{" "}
            <a href="tel: 478348343874387483">478348343874387483</a>
          </p>
          <p>
            Email - <a href="mailto: info@platr.ng">info@platr.ng</a>
          </p>
          <p>
            Website - <a href="/">platr website, we might be on it already</a>
          </p>
        </div>

        <p>
          All enquiries whether made through Email or Phone <br/> would be responded
          to not later than 48hours from time of receipt
        </p>
      </section>
      <section className="form">
        <div className="wid">
          <form className="container ">
            <div className="form-row py-2">
              <label for="inputEmail4">Full Name</label>
              <input type="text" class="form-control" placeholder="Full Name" />
            </div>
            <div className="form-row py-2">
              <label for="inputEmail4">Email address</label>
              <input
                type="text"
                class="form-control"
                placeholder="Email address"
              />
            </div>
            <div className="form-row py-2">
              <label for="inputEmail4">Phone Number</label>
              <input
                type="text"
                class="form-control"
                placeholder="Phone Number"
              />
            </div>
            <div className="form-row py-2">
              <label for="inputEmail4">Message</label>
              <input
                type="textarea"
                class="form-control"
                placeholder="Message"
              />
            </div>
            <div style={{ marginTop: "1rem" }}>
              <CustomButton type="submit" className="lg" text="SEND" />
            </div>
          </form>
        </div>
      </section>
    </div>

        <div className='map'>
          <img className='geozone' src='https://res.cloudinary.com/doo1zd8pn/image/upload/v1581152889/Rectangle_82_ogw4vh.png' alt='map' />
        </div>
    <Footer />
  </div>
);

export default Contact;
