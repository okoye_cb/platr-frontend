import React from "react";
import Navigation from "../../components/Header/newNavBar";
import platrwhite from "../../platr.svg";
import { Radio } from "@material-ui/core";
import {
  MailOutlined,
  PhoneOutlined,
  RoomOutlined,
  CreateOutlined
} from "@material-ui/icons";
import "./index.style.scss";
import Cart from "../../pages/cart/component/Cart";
import Total from "../../pages/cart/component/Total";
import Footer from "../../components/Footer/index";

const ReviewOrder = () => {
  return (
    <div>
      <div style={{ background: "deeppink", width: "auto" }}>
        <div style={{marginTop: '5rem'}}>
          <Navigation
            idName="pink-color"
            brand={platrwhite}
            iconColor="icon-pink"
            bgColor="white"
            
          />
        </div>
      </div>
      <div className="container">
        <h2 className="review-order">Review Order</h2>
        <p className="section-one">1. Select your delivery information:</p>
        <div className="delivery--information">
          <p className="delivery--information__username">
            {" "}
            <Radio color="default" size="small" />
            Mfonobong Umondia
          </p>
          <p className="delivery--information__address">
            {" "}
            <RoomOutlined /> No 20 Udoette Off Ikpa Road University of Kaduna,
            Kaduna State
          </p>
          <p className="delivery--information__mobile">
            {" "}
            <PhoneOutlined /> +234 33 333 3333
          </p>
          <div className="split-items">
            <div className="split-item">
              <p className="delivery--information__email">
                {" "}
                <MailOutlined /> umondiamfonobong@gmail.com
              </p>
            </div>
            <div className="split-imme">
              <p className="delivery--information__edit">
                <CreateOutlined /> Edit
              </p>
            </div>
          </div>
        </div>
        <p className="section-one two">2. Review Order:</p>
        <div className="review border">
          <Cart />
          <div className="grand-total">
            <Total />
          </div>
          
        </div>
        <p className="section-one three">3. Payment:</p>
        <div className="payment-method border container">
            <p className="pay-with-card">Pay with Card:</p>
            <img src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1582198458/payment_gateway_zpzgaq.png" className="img-responsive" height="50" alt="paymentcard"/>
            
        </div>
        <div className="btn-proceed">
        <button className="proceed-checkout">Proceed to Checkout</button>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default ReviewOrder;
