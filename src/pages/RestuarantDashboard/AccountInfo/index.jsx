import React from "react";
import CustomButton from "../../../components/Button";
import Collapsible from "react-collapsible";
import "./accountInfo.scss";

const AccountInfo = () => (
  <div className="account-info">
    <div className="container">
      <section className="main-info">
        <h4 className="info-h4 mt-4">Profile Information</h4>
        <div className="update-picture">
          <img
            src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1583773722/Ellipse_1_ns8tys.png"
            alt="platr"
            className="img-responsive blurry-img"
          />
          <div className="icon">
            <label htmlFor="restuarantPicture">
              <img
                src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1583765719/Vector_hdst6j.png"
                alt="icon"
                className="update-img"
              />
            </label>
            <input
              id="restuarantPicture"
              style={{ display: "none" }}
              type={"file"}
              // onChange={this.fileSelectedHandler}
            />
          </div>
        </div>
        <form className="update-form">
          <input
            type="text"
            className="update-input"
            placeholder="Restaurant Name"
          />
          <input
            type="text"
            className="update-input"
            placeholder="Phone Number"
          />
          <select name="" id="" className="update-input">
            <option value="">Select your Region</option>
            <option value="Kaduna South">Kaduna South</option>
            <option value="Kaduna North">Kaduna North</option>
          </select>
          <input
            type="text"
            className="update-input"
            placeholder="Restaurant Address"
          />
          <input type="email" className="update-input" placeholder="Email" />
          <input
            type="text"
            className="update-input"
            placeholder="peperestaurant.platr.com"
          />
          <textarea
            placeholder="About Restaurant"
            className="update-textarea"
            cols="10"
            rows="8"
          ></textarea>
        </form>
        <CustomButton text="Update" className="no-radius lg" />

        <section className="update-password">
          <Collapsible
            trigger="Change Password"
            className="change-pass"
            triggerWhenOpen="Change Password"
          >
            <input
              type="password"
              className="password-input"
              placeholder="Old Password"
            />
            <input
              type="password"
              className="password-input"
              placeholder="New Password"
            />
            <input
              type="password"
              className="password-input"
              placeholder="Confirm  New Password"
            />
            <input
              type="email"
              className="password-input"
              placeholder="Password Recovery Email"
            />
            <p className="delete-acc">Delete my account</p>
            <CustomButton text="Update" className="no-radius lg" />
          </Collapsible>
        </section>
      </section>
    </div>
  </div>
);

export default AccountInfo;
