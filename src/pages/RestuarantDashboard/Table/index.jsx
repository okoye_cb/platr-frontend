import React from "react";
import "./table.scss";

const TableData = ({
  imgLink,
  userName,
  orderId,
  item,
  qty,
  price,
  date,
  status,
}) => (
  <table className="orders-table">
    <tbody className="tbody">
      <tr>
        <td className="tbody-items name">
          <div className="name-wrap">
            <div className="user-avatar">
              <img src={imgLink} alt="avatar" />
            </div>
            <div className="username-id">
              <p className="name-id">
                {userName} <br />
                <span className="idd">ID: {orderId}</span>
              </p>
            </div>
          </div>
        </td>
        <td className="tbody-items item">{item}</td>
        <td className="tbody-items">{qty}</td>
        <td className="tbody-items">#{price}</td>
        <td className="tbody-items date">{date}</td>
        <td className="tbody-items status">
          <select name="" id="" className="status__options stat">
            {status.map((val) => (
              <option value={val} key={val} className="ready">
                {val}
              </option>
            ))}
          </select>
        </td>
      </tr>
    </tbody>
  </table>
);

export default TableData;
