const data = [
  {
    id: 1,
    imgLink:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584351240/Group_2_njzg3j.png",
    userName: " Mfonobong Umondia",
    orderId: "234567897",
    item: "Pizza and Chips",
    qty: 12,
    price: 5000,
    date: "25th May 2020",
    status: ["Delivered", "Ready", "In-progress", "Canceled", "Confirmed"],
  },
  {
    id: 2,
    imgLink:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584351240/Group_2_njzg3j.png",
    userName: " Mfonobong Umondia",
    orderId: "234567897",
    item: "Pizza and Chips",
    qty: 12,
    price: 5000,
    date: "25th May 2020",
    status: ["Delivered", "Ready", "In-progress", "Canceled", "Confirmed"],
  },
  {
    id: 3,
    imgLink:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584351240/Group_2_njzg3j.png",
    userName: " Mfonobong Umondia",
    orderId: "234567897",
    item: "Pizza and Chips",
    qty: 12,
    price: 5000,
    date: "25th May 2020",
    status: ["Delivered", "Ready", "In-progress", "Canceled", "Confirmed"],
  },
  {
    id: 4,
    imgLink:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584351240/Group_2_njzg3j.png",
    userName: " Mfonobong Umondia",
    orderId: "234567897",
    item: "Pizza and Chips",
    qty: 12,
    price: 5000,
    date: "25th May 2020",
    status: ["Delivered", "Ready", "In-progress", "Canceled", "Confirmed"],
  },
  {
    id: 5,
    imgLink:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584351240/Group_2_njzg3j.png",
    userName: " Mfonobong Umondia",
    orderId: "234567897",
    item: "Pizza and Chips",
    qty: 12,
    price: 5000,
    date: "25th May 2020",
    status: ["Delivered", "Ready", "In-progress", "Canceled", "Confirmed"],
  },
  {
    id: 6,
    imgLink:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584351240/Group_2_njzg3j.png",
    userName: " Mfonobong Umondia",
    orderId: "234567897",
    item: "Pizza and Chips",
    qty: 12,
    price: 5000,
    date: "25th May 2020",
    status: ["Delivered", "Ready", "In-progress", "Canceled", "Confirmed"],
  },
];

export default data;
