import React from "react";
import "./table.scss";

const TableHead = ({ head1, head2, head3, head4, head5, head6 }) => (
  <table className="orders-table">
    <thead className="thead">
      <tr>
        <td className="thead-items name">{head1}</td>
        <td className="thead-items item">{head2}</td>
        <td className="thead-items">{head3}</td>
        <td className="thead-items">{head4}</td>
        <td className="thead-items date">{head5}</td>
        <td className="thead-items status">{head6}</td>
      </tr>
    </thead>
  </table>
);

export default TableHead;
