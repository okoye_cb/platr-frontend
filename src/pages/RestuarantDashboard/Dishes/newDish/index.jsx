import React from "react";
import Dropzone from "react-dropzone";

import "./newdish.styles.scss";

const NewDish = () => (
  <div>
    <main className="new-dish">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            <h2 className="new-dish--h2">Food Items</h2>
          </div>
          <div className="col-lg-6 col-sm-12 text-right">
            <div className="post-dish mt-2">
              <button className="btn-post-dish" type="submit">
                Post New Dish
              </button>
            </div>
          </div>
        </div>

        <section className="new-dish-form">
          <form action="">
            <input
              type="text"
              placeholder="Dish Name"
              className="new-dish-input"
            />

            <select name="" id="" className="category-select">
              <option value="Amala with Vegetables">
                Amala with Vegetables
              </option>
              <option value="Amala with Vegetables">
                Amala with Vegetables
              </option>
              <option value="Amala with Vegetables">
                Amala with Vegetables
              </option>
              <option value="Amala with Vegetables">
                Amala with Vegetables
              </option>
              <option value="Amala with Vegetables">
                Amala with Vegetables
              </option>
            </select>
            <input
              type="text"
              placeholder="Price Tag"
              className="new-dish-input"
            />

            <Dropzone onDrop={() => alert("acceptedFiles")}>
              {({ getRootProps, getInputProps }) => (
                <section>
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <div className="file-upload">
                      <img
                        src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1584455389/Group_599_oyk2l8.png"
                        alt="upload"
                        className="upload-icon"
                      />

                      <p className="file-text mt-3">
                        Click on the thumbnail to upload or drag and <br /> drop
                        picture on the thumbnail to continue
                      </p>
                    </div>
                  </div>
                </section>
              )}
            </Dropzone>

            <button className="btn-submit-dish" type="submit">
              Post Dish
            </button>
          </form>
        </section>
      </div>
    </main>
  </div>
);

export default NewDish;
