const dummyData = [
  {
    id: 1,
    imgUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584431079/Rectangle_10.15_bkxjj2.png",
    mealName: "Chicken Tandori Special 12 Deep Pan",
    price: "200.00"
  },
  {
    id: 2,
    imgUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584431079/Rectangle_10.15_bkxjj2.png",
    mealName: "Chicken Tandori Special 12 Deep Pan",
    price: "200.00"
  },
  {
    id: 3,
    imgUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584431079/Rectangle_10.15_bkxjj2.png",
    mealName: "Chicken Tandori Special 12 Deep Pan",
    price: "200.00"
  },
  {
    id: 4,
    imgUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584431079/Rectangle_10.15_bkxjj2.png",
    mealName: "Chicken Tandori Special 12 Deep Pan",
    price: "200.00"
  },
  {
    id: 5,
    imgUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584431079/Rectangle_10.15_bkxjj2.png",
    mealName: "Chicken Tandori Special 12 Deep Pan",
    price: "200.00"
  },
  {
    id: 6,
    imgUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1584431079/Rectangle_10.15_bkxjj2.png",
    mealName: "Chicken Tandori Special 12 Deep Pan",
    price: "200.00"
  }
];

export default dummyData;
