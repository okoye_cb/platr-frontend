import React, { Component } from "react";
import SimpleMenu from "./Menu";
import DishesCard from "./dishesCard";
import NewDish from "./newDish";
import dummyData from "./dummyData";
import "./dishes.styles.scss";

class Dishes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dishes: true,
      newDishes: false,
      active: "dishes"
    };
  }

  handleNewDishes = () => {
    this.setState({
      active: "newDishes"
    });
  };

  render() {
    const DishesDefault = () => (
      <main className="dishes">
        <div className="container">
          <h3 className="dishes-h3">Food Items</h3>
          <div className="row">
            <div className="col-lg-5   col-sm-12">
              <div className="dishes-cat-input">
                <input
                  type="text"
                  placeholder="Search Category"
                  className="dishes-id-input"
                />
                <button className="btn-search-dishes" type="submit">
                  Search
                </button>
              </div>
            </div>
            <div className="col-lg-3 col-sm-12 ">
              <div className="select-box">
                <select className="select-cat">
                  <option value="Select Category">Select Category</option>
                </select>
              </div>
            </div>
            <div className="col-lg-2 col-sm-12 offset-lg-2">
              <button
                className="btn-add-dishes"
                type="submit"
                onClick={() => this.handleNewDishes()}
              >
                Add New Dish
              </button>
            </div>
          </div>

          <section className="dishes-wrapper">
            <div className="top-wrapper">
              <h3 className="dishes-name-h3">Assorted African Made</h3>
              <div className="hidden-content ">
                <SimpleMenu />
              </div>
            </div>

            <div className="row">
              {dummyData.map(dummy => (
                <div className="col-lg-3 col-sm-12" key={dummy.id}>
                  <DishesCard
                    imgUrl={dummy.imgUrl}
                    mealName={"Chicken Tandori Special 12 Deep Pan"}
                    price={"200.00"}
                  />
                </div>
              ))}

              <div className="col-lg-3 col-sm-12">
                <div className="add-dish-icon mt-5">
                  <img
                    src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1584430839/Group_630_qcylfa.png"
                    alt=""
                    className="new-dish-icon"
                    onClick={() => this.handleNewDishes()}
                  />
                </div>
              </div>
            </div>
          </section>
        </div>
      </main>
    );

    return (
      <div>
        {this.state.active === "dishes" && <DishesDefault />}
        {this.state.active === "newDishes" && (
          <NewDish handleNewDishes={this.handleNewDishes} />
        )}
      </div>
    );
  }
}

export default Dishes;
