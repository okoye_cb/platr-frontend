import React from "react";
import SimpleMenu from "./Menu";
import "./dishes.styles.scss";

const DishesCard = ({ imgUrl, mealName, price }) => (
  <div className="dishes-card">
    <div className="dishes-img">
      <img src={imgUrl} alt="" className="dish-avatar" />
    </div>
    <div className="dishes-name">{mealName}</div>
    <div className="hidden-menu text-right">
      <SimpleMenu />
    </div>
    <img
      src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1584431079/Rectangle_9_z2sqg2.png"
      alt=""
      className="price-tag"
    />
    <span className="price-tag-text">N{price}</span>
  </div>
);

export default DishesCard;
