import React from "react";
import "../Table/table.scss";

const EarningThead = ({ head1, head2, head3, head4, head5 }) => (
  <table className="orders-table">
    <thead className="thead">
      <tr>
        <td className="thead-items name">{head1}</td>
        <td className="thead-items food">{head2}</td>
        <td className="thead-items status">{head3}</td>
        <td className="thead-items">{head4}</td>
        <td className="thead-items date">{head5}</td>
      </tr>
    </thead>
  </table>
);

export default EarningThead;
