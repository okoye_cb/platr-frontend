import React from "react";
import "../Table/table.scss";

const EarningTable = ({ userName, orderId, item, qty, price, date }) => (
  <table className="orders-table">
    <tbody className="tbody">
      <tr>
        <td className="tbody-items name">
          <div className="name-wrap" style={{ marginTop: "-.3em" }}>
            <div className="username-id">
              <p className="name-id">
                {userName} <br />
                <span className="idd">ID: {orderId}</span>
              </p>
            </div>
          </div>
        </td>
        <td className="tbody-items food">{item}</td>
        <td className="tbody-items status">{qty}</td>
        <td className="tbody-items">#{price}</td>
        <td className="tbody-items date">{date}</td>
      </tr>
    </tbody>
  </table>
);

export default EarningTable;
