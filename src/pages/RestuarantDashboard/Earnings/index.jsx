import React from "react";
import EarningThead from "./earningThead";
import EarningTable from "./earningTable";
import data from "../Table/dataTableContent";
import "./earnings.styles.scss";

const Earnings = () => (
  <main className="earning-wrapper">
    <div className="container">
      <h3 className="earning-h3">Earnings</h3>
      <section className="earning-data">
        <div className="earning-badge">
          <div className="total-text">Total Earnings</div>
          <div className="total-figure"> #600,000.00</div>
        </div>

        <section className="filter-box">
          <div className="row">
            <div className="col-lg-3 offset-lg-9 col-sm-12">
              <div className="filter-input">
                <select className="select-filter">
                  <option value="Select Category">Filter by Date</option>
                </select>
              </div>
            </div>
          </div>
        </section>

        <section className="table">
          <EarningThead
            head1={"Name"}
            head2={"Food Item"}
            head3={"Qty"}
            head4={"Value"}
            head5={"Date"}
          />
          {data.map(earning => (
            <EarningTable
              key={earning.id}
              userName={earning.userName}
              orderId={earning.orderId}
              item={earning.item}
              qty={earning.qty}
              date={earning.date}
              price={earning.price}
            />
          ))}
        </section>
      </section>
    </div>
  </main>
);

export default Earnings;
