import React from "react";
import Collapsible from "react-collapsible";
import TableData from "../Table";
import TableHead from "../Table/thead";
import data from "../Table/dataTableContent";
import "./orderInfo.styles.scss";

const OrdersInformation = () => (
  <main className="orders-info">
    <div className="container">
      <section className="order-top">
        <h2 className="orders-h2 mt-2 mb-3">Orders</h2>
        <div className="row">
          <div className="col-lg-6  col-sm-12">
            <div className="order-info-input">
              <input
                type="text"
                placeholder="Order Id"
                className="order-id-input"
              />
              <button className="btn-search-order" type="submit">
                Search
              </button>
            </div>
          </div>
          <div className="col-lg-4 offset-lg-2 col-sm-12">
            <div className="select-box">
              <select className="select-cat">
                <option value="Select Category">Select Category</option>
              </select>
            </div>
          </div>
        </div>
      </section>

      <section className="all-orders-wrapper">
        <Collapsible
          trigger="All Orders"
          className="all-orders"
          triggerWhenOpen="All Orders"
          open={true}
        >
          <TableHead
            head1={"Name"}
            head2={"Item"}
            head3={"Qty"}
            head4={"Price"}
            head5={"Date"}
            head6={"Status"}
          />
          {data.map(dummy => (
            <TableData
              key={dummy.id}
              imgLink={dummy.imgLink}
              userName={dummy.userName}
              orderId={dummy.orderId}
              price={dummy.price}
              item={dummy.item}
              qty={dummy.qty}
              status={dummy.status}
              date={dummy.date}
            />
          ))}
        </Collapsible>
      </section>

      <section className="trending-orders-wrapper">
        <Collapsible
          trigger="Trending Orders"
          className="trending-orders"
          triggerWhenOpen="Trending Orders"
        >
          <TableHead
            head1={"Name"}
            head2={"Item"}
            head3={"Qty"}
            head4={"Price"}
            head5={"Date"}
            head6={"Status"}
          />
          {data.map(dummy => (
            <TableData
              key={dummy.id}
              imgLink={dummy.imgLink}
              userName={dummy.userName}
              orderId={dummy.orderId}
              price={dummy.price}
              item={dummy.item}
              qty={dummy.qty}
              status={dummy.status}
              date={dummy.date}
            />
          ))}
        </Collapsible>
      </section>
      <section className="orders-awaiting-wrapper">
        <Collapsible
          trigger="Orders Awaiting Payment"
          className="awaiting-orders"
          triggerWhenOpen="Orders Awaiting Payment"
        >
          <TableHead
            head1={"Name"}
            head2={"Item"}
            head3={"Qty"}
            head4={"Price"}
            head5={"Date"}
            head6={"Status"}
          />
          {data.map(dummy => (
            <TableData
              key={dummy.id}
              imgLink={dummy.imgLink}
              userName={dummy.userName}
              orderId={dummy.orderId}
              price={dummy.price}
              item={dummy.item}
              qty={dummy.qty}
              status={dummy.status}
              date={dummy.date}
            />
          ))}
        </Collapsible>
      </section>
      <section className="cancelled-orders-wrapper">
        <Collapsible
          trigger="Cancelled Order"
          className="cancelled-orders"
          triggerWhenOpen="Cancelled Order"
        >
          <TableHead
            head1={"Name"}
            head2={"Item"}
            head3={"Qty"}
            head4={"Price"}
            head5={"Date"}
            head6={"Status"}
          />
          {data.map(dummy => (
            <TableData
              key={dummy.id}
              imgLink={dummy.imgLink}
              userName={dummy.userName}
              orderId={dummy.orderId}
              price={dummy.price}
              item={dummy.item}
              qty={dummy.qty}
              status={dummy.status}
              date={dummy.date}
            />
          ))}
        </Collapsible>
      </section>
      <section className="delivered-orders-wrapper">
        <Collapsible
          trigger="Delivered Order"
          className="delivered-orders"
          triggerWhenOpen="Delivered Order"
        >
          <TableHead
            head1={"Name"}
            head2={"Item"}
            head3={"Qty"}
            head4={"Price"}
            head5={"Date"}
            head6={"Status"}
          />
          {data.map(dummy => (
            <TableData
              key={dummy.id}
              imgLink={dummy.imgLink}
              userName={dummy.userName}
              orderId={dummy.orderId}
              price={dummy.price}
              item={dummy.item}
              qty={dummy.qty}
              status={dummy.status}
              date={dummy.date}
            />
          ))}
        </Collapsible>
      </section>
      <section className="orders-awaiting-shipment">
        <Collapsible
          trigger="Orders Awaiting Shipment"
          className="shipment-orders"
          triggerWhenOpen="Orders Awaiting Shipment"
        >
          <TableHead
            head1={"Name"}
            head2={"Item"}
            head3={"Qty"}
            head4={"Price"}
            head5={"Date"}
            head6={"Status"}
          />
          {data.map(dummy => (
            <TableData
              key={dummy.id}
              imgLink={dummy.imgLink}
              userName={dummy.userName}
              orderId={dummy.orderId}
              price={dummy.price}
              item={dummy.item}
              qty={dummy.qty}
              status={dummy.status}
              date={dummy.date}
            />
          ))}
        </Collapsible>
      </section>
    </div>
  </main>
);

export default OrdersInformation;
