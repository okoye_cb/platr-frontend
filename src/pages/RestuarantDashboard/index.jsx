import React, { Component } from "react";
import Navigation from "../../components/Header/newNavBar";
import platrwhite from "../../platr.svg";
import Footer from "../../components/Footer/index";
import AccountInfo from "./AccountInfo";
import OrdersInfromation from "./OrdersInfo";
import Earnings from "./Earnings";
import Dishes from "./Dishes";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.styles.scss";

class RestaurantDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: true,
      orders: false,
      dishes: false,
      earnings: false,
      active: "account"
    };
    //confirms token in url testing
    
    console.log(props)
  }

  handleAccountInfo = () => {
    this.setState({
      active: "account"
    });
  };

  handleOrders = () => {
    this.setState({
      active: "orders"
    });
  };
  handleDishes = () => {
    this.setState({
      active: "dishes"
    });
  };
  handleEarnings = () => {
    this.setState({
      active: "earnings"
    });
  };

  render() {
    const accountActive =
      this.state.active === "account" ? "border-b" : "no-border-b";
    const orderActive =
      this.state.active === "orders" ? "border-b" : "no-border-b";
    const dishesActive =
      this.state.active === "dishes" ? "border-b" : "no-border-b";
    const earningActive =
      this.state.active === "earnings" ? "border-b" : "no-border-b";

      
    return (
      <div className=" restaurant-dashboard">
        <header>
          <div
            style={{
              background: "deeppink",
              width: "100vw",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)"
            }}
          >
            <div>
              <Navigation
                idName="pink-color"
                brand={platrwhite}
                iconColor="icon-pink"
                bgColor="white"
                token = {this.props.match.params.token}
                              />
            </div>
          </div>
          <div className="container">
            <div className="restaurant-profile pt-5">
              <div className="restaurant-picture">
                <img
                  src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1584023310/Ellipse_wjogaz.png"
                  alt="platr-restaurant"
                  className="img-responsive restaurant-avatar"
                />
              </div>
              <div className="restaurant-name mt-3">Pepe's Restaurant</div>
            </div>
            <main className="main-dashboard">
              <section className="restaurant-heading mt-5">
                <div
                  className={`heading-tab mr-5 `}
                  onClick={() => this.handleAccountInfo()}
                >
                  Account Information
                  <div className={`${accountActive}`}></div>
                </div>
                <div
                  className={`heading-tab`}
                  onClick={() => this.handleOrders()}
                >
                  Orders Information
                  <div className={`${orderActive}`}></div>
                </div>
                <div
                  className={`heading-tab`}
                  onClick={() => this.handleDishes()}
                >
                  Add Dishes
                  <div className={`${dishesActive}`}></div>
                </div>
                <div
                  className={`heading-tab`}
                  onClick={() => this.handleEarnings()}
                >
                  Earnings
                  <div className={`${earningActive}`}></div>
                </div>
              </section>

              {this.state.active === "account" && (
                <AccountInfo handleAccountInfo={this.handleAccountInfo} />
              )}
              {this.state.active === "orders" && (
                <OrdersInfromation handleOrders={this.handleOrders} />
              )}
              {this.state.active === "dishes" && (
                <Dishes handleDishes={this.handleDishes} />
              )}
              {this.state.active === "earnings" && (
                <Earnings handleEarnings={this.handleEarnings} />
              )}
            </main>
          </div>
        </header>
        <Footer />
      </div>
    );
  }
}

export default RestaurantDashboard;
