import React from "react";
import "./styles.scss";

const BlogCard = ({ topImageUrl, imageUrl, topHeading, topText }) => (
  <div className="col-lg-4 col-md-6 col-sm-12">
    <div className="blog__cards">
      <div className="blog--image">
        <img
          src={topImageUrl || imageUrl}
          alt="img"
          className="img-responsive blog-img"
        />
      </div>
      <h6 className="card--heading">{topHeading}</h6>
      <p className="card-text">{topText}</p>
    </div>
  </div>
);

export default BlogCard;
