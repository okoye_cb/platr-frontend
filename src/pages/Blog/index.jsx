import React from "react";
import MainHeader from "../../components/PagesHeader/MainHeader";
import Navigation from "../../components/Header/newNavBar";
import TitleAndDescription from "../../components/PagesHeader/Title";
import Footer from "../../components/Footer";
import dummyData from "./data";
import BlogCard from "./Card";
import "./index.styles.scss";
import platrwhite from "../../platr.svg";

const Blog = () => (
  <div className="blog">
    <MainHeader
      renderProp={() => (
        <div>
          <Navigation
            idName={"pink-color"}
            brand={platrwhite}
            bgColor={'white'}
            iconColor={'icon-pink'}
            
          />
          <TitleAndDescription title={"Blog"} />
          <div className="center text-center">
            <p>
              These are the restaurants in the selected location, you can choose{" "}
              <br />
              another location to show restaurants in another city.
            </p>
          </div>
        </div>
      )}
    />
    <main className="blog-wrapper">
      <div className="container">
        <div className="row">
          <div className="col-lg-9 col-md-8 col-sm-7 col-xs-12">
            <h4 className="top-read__h4 width">Top Read Articles</h4>
            <section className="top-read-wrap left">
              <div className="blog__card-wrap">
                <div className="row">
                  {dummyData
                    .map(dummy => (
                      <BlogCard
                        key={dummy.id}
                        topImageUrl={dummy.topImageUrl}
                        topHeading={dummy.topHeading}
                        topText={dummy.topText}
                      />
                    ))
                    .splice(0, 3)}
                </div>
              </div>
            </section>
            <section className="blog-form">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <div className="form-img">
                    <img
                      src="https://res.cloudinary.com/doo1zd8pn/image/upload/c_scale,h_120,w_410/v1581958679/Rectangle_3.11_mxjc2d.png"
                      alt=""
                      className="img-responsive img-side"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <div className="form-bg">
                    <h5 className="form-h5">
                      Get articles directly in your inbox
                    </h5>
                    <form action="" className="bg-form">
                      <input
                        type="email"
                        placeholder="Enter email address"
                        className="input-sub"
                      />
                      <button className="btn-sub">Subscribe now</button>
                    </form>
                  </div>
                </div>
              </div>
            </section>
            <section className="bottom-post">
              <h4 className="bottom-post__h4 text-center">Latest Articles</h4>
              <div className="row">
                {dummyData
                  .map(dummy => (
                    <BlogCard
                      key={dummy.id}
                      topImageUrl={dummy.imageUrl}
                      topHeading={dummy.topHeading}
                      topText={dummy.topText}
                    />
                  ))
                  .splice(3)}
              </div>
            </section>
          </div>
          <div className="col-lg-3 col-md-4 col-sm-5 col-xs-12">
            <h4 className="top-read__h4">Top Read Articles</h4>
            <section className="top-read-wrap right">
              {dummyData.map(dummy => (
                <p className="right-texts" key={dummy.id}>
                  {dummy.leftArticles}
                </p>
              ))}
            </section>
          </div>
        </div>
      </div>
      <div className="img-pos">
        <img
          src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1581420455/image14_mmck3x.svg"
          alt="spicy"
        />
      </div>
    </main>

    <Footer />
  </div>
);

export default Blog;
