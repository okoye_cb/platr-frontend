const dummyData = [
  {
    id: 1,
    topImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955082/Rectangle_4_1_yyiwch.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 2,
    topImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955079/Rectangle_4.1_1_zwytji.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 3,
    topImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955080/Rectangle_4.2_1_my4ruj.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 4,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955082/Rectangle_4_yyyjut.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 5,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955080/Rectangle_4.1_vm3ovl.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 6,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955079/Rectangle_4.2_cvzoju.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 7,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955082/Rectangle_4_2_s8ohjo.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 8,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955079/Rectangle_4.1_2_majkhw.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 9,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955083/Rectangle_4.2_2_zvbacq.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 10,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955082/Rectangle_4_yyyjut.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 11,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955080/Rectangle_4.1_vm3ovl.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 12,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955079/Rectangle_4.2_cvzoju.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 13,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955082/Rectangle_4_2_s8ohjo.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 14,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955079/Rectangle_4.1_2_majkhw.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  },
  {
    id: 15,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1581955083/Rectangle_4.2_2_zvbacq.png",
    topHeading: "Lorem ipsum dolo",
    topText:
      " consectetur adipiscing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.",
    leftArticles: "consectetur adipiscing elit, sed do eiusmod"
  }
];

export default dummyData;
