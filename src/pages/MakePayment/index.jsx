import React from "react";
import { ReactComponent as PlatrWhite } from "../../platrwhite.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";

import { connect } from 'react-redux';
import { selectCartTotal } from '../../redux/cartReducer/cart.selectors'
import { Link } from "react-router-dom";
import "./makePayment.styles.scss";

const MakePayment = ({total}) => (
  <React.Fragment>
    <header className="make-header">
      <div className="container-fluid">
        <div className="row">
          <Link to="/">
            <PlatrWhite className="brand" />
          </Link>
        </div>
      </div>
    </header>
    <div className="make-payment">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-sm-12">
            <section className="order-summary">
              <div className="top">
                <p className="top__items">Order Summary</p>
                <p className="top__items">
                  <Link to="/cart" className="link">
                    See Details &nbsp;&nbsp;
                    <FontAwesomeIcon
                      icon={faChevronRight}
                      size="sm"
                      color="rgba(0, 0, 0, 0.6)"
                    />
                  </Link>
                </p>
              </div>
              <div className="total-amount">
                <p className="amount">Total Amount</p>
                <p className="amount">&#x20A6; {total}</p>
              </div>
            </section>
          </div>

          <div className="col-lg-12 col-sm-12">
            <p className="pay-card-heading">Enter Delivery Details</p>

            <section className="pay-with-form">
              <form action="" className="pay-form-wrap">
                <input
                  type="email"
                  className="pay-input"
                  placeholder="Email Address"
                />

                <input
                  type="number"
                  className="pay-input"
                  placeholder="Phone Number"
                />
                <textarea
                  name=""
                  id=""
                  cols="30"
                  rows="10"
                  placeholder="Delivery Address"
                  className="pay-textarea"
                ></textarea>
                <button className="btn-pay-now" type="button">
                  Proceed
                </button>
              </form>
            </section>
          </div>
          <div className="col-lg-12 col-sm-12">
            <p className="agree text-center mt-3">
              By clicking on "PROCEED" you have agreed to our{" "}
              <Link to="/terms">Terms and Conditions</Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  </React.Fragment>
);

const mapStateToProps = (state) => ({
  total: selectCartTotal(state)
});

export default connect(mapStateToProps) (MakePayment);
