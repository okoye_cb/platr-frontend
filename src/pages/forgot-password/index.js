import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import  { connect } from 'react-redux'
import CustomButton from "../../components/Button";
import Navigation from "../../components/Header/newNavBar";
import "./forgotpassword.scss";
import platrwhite from "../../platr.svg";
import { resetPassword } from '../../redux/user/signup.action'

const ForgotPassword = (props) => {
  
    const [email, getEmail] = useState({
      email: ''
    })

    const updateField = (e) => {
      getEmail({
        ...email,
        [e.target.name]: e.target.value,
      });
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      props.resetPassword(email)
    }

 return (

  <div className="access">
    <div style={{ background: "deeppink", width: "100vw" }}>
      <div>
        <Navigation
          idName="pink-color"
          brand={platrwhite}
          iconColor="icon-pink"
          bgColor="transparent"
          
        />
      </div>
    </div>
    <div className="border">
      <div>
        <h3 className="reset">Reset your password</h3>
        <p className="text">Lorem ipsum dolor sit amet, consectetur </p>
        <p className="text"> adipiscing elit, sed do </p>
      </div>
      <form className="forgot-form">
        <div className="container py-4">
          <div className="form-row ">
            <label for="inputEmail4">Email address</label>
            <input
              type="text"
              class="form-control"
              placeholder="input email address"
              value={email.email}
              onChange={ updateField }
              name='email'
            />
          </div>
        </div>
        <div className="button ">
          <CustomButton 
          disabled={ !email.email }
            className="lg" 
              type="submit" 
                text={"RESET"}
                  onClick= {handleSubmit}
                />
        </div>
      </form>
    </div>
  </div>
)};

export default connect(null, {resetPassword})(ForgotPassword);
