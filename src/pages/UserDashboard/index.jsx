import React, { Component } from "react";
import Navigation from "../../components/Header/newNavBar";
import platrwhite from "../../platr.svg";
import Footer from "../../components/Footer/index";
import "bootstrap/dist/css/bootstrap.min.css";
import History from "./History";
import AccountInfo from "./AccountInfo";
import "./index.styles.scss";

class UserDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: true,
      history: false
    };
  }

  
  handleAccountInfo = () => {
    this.setState({ account: !this.state.history, history: false });
  };

  handleHistory = () => {
    this.setState({ history: !this.state.account, account: false });
  };

  render() {
    const borderBottom = this.state.account ? "border-b" : "no-border-b";
    const noBorderBottom = !this.state.account ? "border-b" : "no-border-b";
    
    const test = window.localStorage.getItem("currentUser");
    const newTest = JSON.parse(test);

    const userJSON = localStorage.getItem("user");
    const userObj = JSON.parse(userJSON);
  
    const usersigninJSON = localStorage.getItem("usersignin");
    const usersigninObj = JSON.parse(usersigninJSON);

  
    return (
      <div className=" user-dashboard">
        <header>
          <div
            style={{
              background: "deeppink",
              width: "100vw",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)"
            }}
          >
            <div>
              <Navigation
                idName="pink-color"
                brand={platrwhite}
                iconColor="icon-pink"
                bgColor="white"
                
              />
            </div>
          </div>
          <div className="container">
            <div className="user-profile pt-5">
              <div className="user-picture">
                <img
                  src={newTest?.photo}
                  alt="platr-user"
                  className="img-responsive user-avatar"
                />
              </div>
              <div className="user-name mt-3">
              {test
                  ? <h3> <span style={{color: '#eb2188', fontWeight: 'bold'}}>Welcome,</span> {newTest.name}</h3>
                  : usersigninObj
                  ? <h3> <span style={{color: '#eb2188', fontWeight: 'bold'}}>Welcome,</span> {usersigninObj.userDetails.firstName}</h3>
                  : userObj
                  ? <h3> <span style={{color: '#eb2188', fontWeight: 'bold'}}>Welcome,</span> {userObj.user.firstName}</h3> 
                  :<h3>Please Login</h3>
                 }
              </div>
            </div>
            <main className="main-dashboard">
              <section className="user-heading mt-5">
                <div
                  className={`heading-tab mr-5 `}
                  onClick={() => this.handleAccountInfo()}
                >
                  Account Information
                  <div className={`${borderBottom}`}></div>
                </div>
                <div
                  className={`heading-tab`}
                  onClick={() => this.handleHistory()}
                >
                  History
                  <div className={`${noBorderBottom}`}></div>
                </div>
              </section>
              {this.state.account ? (
                <AccountInfo handleAccountInfo={this.handleAccountInfo} />
              ) : (
                <History handleHistory={this.handleHistory} />
              )}
            </main>
          </div>
        </header>
        <Footer />
      </div>
    );
  }
}

export default UserDashboard;
