import React from "react";
import CustomButton from "../../../components/Button";
import Collapsible from "react-collapsible";
import "./accountInfo.styles.scss";

const test = window.localStorage.getItem("currentUser");
    const newTest = JSON.parse(test);

    const userJSON = localStorage.getItem("user");
    const userObj = JSON.parse(userJSON);
  
    const usersigninJSON = localStorage.getItem("usersignin");
    const usersigninObj = JSON.parse(usersigninJSON);



    

    
const AccountInfo = () => (
  <div className="account-info">
    <div className="container">
      <section className="main-info">
        <h4 className="info-h4 mt-4">Profile Information</h4>
        <div className="update-picture">
          <img
            src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1583773722/Ellipse_1_ns8tys.png"
            alt="platr"
            className="img-responsive blurry-img"
          />
          <div className="icon">
            <label htmlFor="userPicture">
              <img
                src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1583765719/Vector_hdst6j.png"
                alt="icon"
                className="update-img"
              />
            </label>
            <input
              id="userPicture"
              style={{ display: "none" }}
              type={"file"}
              // onChange={this.fileSelectedHandler}
            />
          </div>
        </div>
        <form className="update-form">
          <input type="text" className="update-input" placeholder={test
                  ? `${newTest.name.toUpperCase()}`
                  : usersigninObj
                  ? `${usersigninObj.userDetails.firstName.toUpperCase()}`
                  : userObj
                  ? `${userObj.user.firstName.toUpperCase()}` 
                  :"User"
                 } />
          <input type="text" className="update-input" placeholder={test
                  ? `${newTest.name.toUpperCase()}`
                  : usersigninObj
                  ? `${usersigninObj.userDetails.lastName.toUpperCase()}`
                  : userObj
                  ? `${userObj.user.lastName.toUpperCase()}` 
                  :"User"
                 } />
          <input type="text" className="update-input" placeholder="Address" />
          <input type="email" className="update-input" placeholder={test
                  ? `${newTest?.email}`
                  : usersigninObj
                  ? `${usersigninObj.userDetails.email}`
                  : userObj
                  ? `${userObj.user.email}` 
                  :"User"
                 } />
                  <input type="email" className="update-input" placeholder={test
                  ? `${newTest?.phone}`
                  : usersigninObj
                  ? `${usersigninObj.userDetails.phone}`
                  : userObj
                  ? `${userObj.user.phone}` 
                  :"User"
                 } />
          <input
            type="text"
            className="update-input"
            placeholder="Delivery Address"
          />
        </form>
        <CustomButton text="Update" className="no-radius lg" />

        <section className="update-password">
          <Collapsible
            trigger="Change Password"
            className="change-pass"
            triggerWhenOpen="Change Password"
          >
            <input
              type="password"
              className="password-input"
              placeholder="Old Password"
            />
            <input
              type="password"
              className="password-input"
              placeholder="New Password"
            />
            <input
              type="password"
              className="password-input"
              placeholder="Confirm  New Password"
            />
            <input
              type="email"
              className="password-input"
              placeholder="Password Recovery Email"
            />
            <p className="delete-acc">Delete my account</p>
            <CustomButton text="Update" className="no-radius lg" />
          </Collapsible>
        </section>
      </section>
    </div>
  </div>
);

export default AccountInfo;
