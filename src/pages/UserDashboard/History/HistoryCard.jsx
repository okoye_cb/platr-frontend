import React from "react";
import "./history.styles.scss";


const HistoryCard = ({
  restaurantName,
  mealImageUrl,
  firstMeal,
  secondMeal,
  orderId,
  quantity,
  price,
  deliveryTime,
  deliveryCost,
  deliveryStatus
}) => (
  <section className="history-details">
    <h6 className="rest-name">Restaurant: {restaurantName}</h6>
    <div className="row">
      <div className="col-lg-6 col-sm-12">
        <div className="pro-name-details">
          <h5 className="pro-name--h5">Product Name & Details</h5>
          <div className="product-details">
            <div className="rest rest-image mr-3">
              <img
                src={mealImageUrl}
                alt=""
                className="img-responsive rest-image-avatar"
              />
            </div>
            <div className="rest rest-details">
              <h5 className="rest--h5">{firstMeal}</h5>
              <h6 className="rest--h6">{secondMeal}</h6>
              <p className="rest-id mt-4">ID :{orderId}</p>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-6 col-sm-12">
        <div className="product-quanity">
          <div className="quantity-items">
            <h5 className="quantity-h5">Quantity</h5>

            <p className="sub-quan text-center">{quantity}</p>
          </div>
          <div className="quantity-items">
            <h5 className="quantity-h5">Price</h5>
            <p className="sub-quan">{price}</p>
          </div>
          <div className="quantity-items">
            <h5 className="quantity-h5">Delivery Details</h5>
            <p className="delivery-time">Delivery Time: {deliveryTime}</p>
            <p className="delivery-price text-right">
              Delivery: #{deliveryCost}
            </p>
            {deliveryStatus === "Canceled" ? (
              <p className="delivery-status red text-right">{deliveryStatus}</p>
            ) : (
              <p className="delivery-status green text-right">
                {deliveryStatus}
              </p>
            )}
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default HistoryCard;
