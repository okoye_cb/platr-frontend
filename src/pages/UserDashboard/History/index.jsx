import React from "react";
import HistoryCard from "./HistoryCard";
import data from "./historyData";
import "./history.styles.scss";

const History = () => (
  <div className="history">
    <div className="container">
      <div className="main-history">
        <div className="row">
          <div className="col-lg-6  col-sm-12">
            <div className="history-order">
              <input
                type="text"
                placeholder="Order Id"
                className="order-id-input"
              />
              <button className="btn-search-order" type="submit">
                Search
              </button>
            </div>
          </div>
          <div className="col-lg-4 offset-lg-2 col-sm-12">
            <div className="select-box">
              <select className="select-cat">
                <option value="Select Category">Select Category</option>
              </select>
            </div>
          </div>
        </div>
        <section className="history-details-wrapper">
          {data.map(item => (
            <HistoryCard
              key={item.id}
              restaurantName={item.restaurantName}
              mealImageUrl={item.mealImageUrl}
              orderId={item.orderId}
              firstMeal={item.firstMeal}
              secondMeal={item.secondMeal}
              quantity={item.quantity}
              price={item.price}
              deliveryTime={item.deliveryTime}
              deliveryCost={item.deliveryCost}
              deliveryStatus={item.deliveryStatus}
            />
          ))}
        </section>
      </div>
      <section className="pagination">
        <span className="box pt-1">&lt;</span>
        <span className="pagination-text mt-1"> Page 2 fo 3</span>
        <span className="box pt-1">&gt;</span>
      </section>
    </div>
  </div>
);

export default History;
