const data = [
  {
    id: 1,
    mealImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1583851632/Rectangle_4.3_qpbzbx.png",
    restaurantName: "Pepe's Restaurant",
    firstMeal: "Yam Chips and Burger",
    secondMeal: "Yam, minced meat and italian herbs",
    orderId: 12345677890,
    quantity: 1,
    price: "1,500",
    deliveryTime: "45Min - 1hr",
    deliveryCost: 100,
    deliveryStatus: "Canceled"
  },
  {
    id: 2,
    mealImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1583851632/Rectangle_4.3_qpbzbx.png",
    restaurantName: "Pepe's Restaurant",
    firstMeal: "Yam Chips and Burger",
    secondMeal: "Yam, minced meat and italian herbs",
    orderId: 12345677890,
    quantity: 1,
    price: "1,500",
    deliveryTime: "45Min - 1hr",
    deliveryCost: 100,
    deliveryStatus: "Delivered"
  },
  {
    id: 3,
    mealImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1583851632/Rectangle_4.3_qpbzbx.png",
    restaurantName: "Pepe's Restaurant",
    firstMeal: "Yam Chips and Burger",
    secondMeal: "Yam, minced meat and italian herbs",
    orderId: 12345677890,
    quantity: 1,
    price: "1,500",
    deliveryTime: "45Min - 1hr",
    deliveryCost: 100,
    deliveryStatus: "Canceled"
  },
  {
    id: 4,
    mealImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1583851632/Rectangle_4.3_qpbzbx.png",
    restaurantName: "Pepe's Restaurant",
    firstMeal: "Yam Chips and Burger",
    secondMeal: "Yam, minced meat and italian herbs",
    orderId: 12345677890,
    quantity: 1,
    price: "1,500",
    deliveryTime: "45Min - 1hr",
    deliveryCost: 100,
    deliveryStatus: "Delivered"
  },
  {
    id: 5,
    mealImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1583851632/Rectangle_4.3_qpbzbx.png",
    restaurantName: "Pepe's Restaurant",
    firstMeal: "Yam Chips and Burger",
    secondMeal: "Yam, minced meat and italian herbs",
    orderId: 12345677890,
    quantity: 1,
    price: "1,500",
    deliveryTime: "45Min - 1hr",
    deliveryCost: 100,
    deliveryStatus: "Canceled"
  },
  {
    id: 6,
    mealImageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1583851632/Rectangle_4.3_qpbzbx.png",
    restaurantName: "Pepe's Restaurant",
    firstMeal: "Yam Chips and Burger",
    secondMeal: "Yam, minced meat and italian herbs",
    orderId: 12345677890,
    quantity: 1,
    price: "1,500",
    deliveryTime: "45Min - 1hr",
    deliveryCost: 100,
    deliveryStatus: "Delivered"
  }
];

export default data;
