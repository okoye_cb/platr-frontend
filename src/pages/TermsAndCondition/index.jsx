import React from "react";
import MainHeader from "../../components/PagesHeader/MainHeader";
import Navigation from "../../components/Header/newNavBar";
import TitleAndDescription from "../../components/PagesHeader/Title";
import Footer from "../../components/Footer";
import content from "./data";
import "./styles.scss";
import platrwhite from "../../platr.svg";

const TermsAndContions = () => (
  <div className="terms-and-conditon">
    <MainHeader
      renderProp={() => (
        <div>
          <Navigation
            idName={"pink-color"}
            brand={platrwhite}
            iconColor="icon-pink"
            bgColor="white"
            
          />
          <TitleAndDescription title={"Terms of Use"} />
          <div className="center text-center">
            <p>
              These are the restaurants in the selected location, you can choose{" "}
              <br />
              another location to show restaurants in another city.
            </p>
          </div>
        </div>
      )}
    />

    <main className="content-area">
      <div className="container-fluid">
        <hr />
        <div className="row">
          <div className="col-lg-12 col-sm-12 p-1">
            <div className="wrap">
              <h6 className="date">Last updated December 31st, 2019.</h6>
              {content.map(item => (
                <div className="terms" key={item.id}>
                  <h5 className="h5">{item.heading}</h5>
                  <p className="p">{item.text}</p>
                </div>
              ))}
            </div>
          </div>
          <div className="img">
            <img
              src="https://res.cloudinary.com/doo1zd8pn/image/upload/v1581420455/image14_mmck3x.svg"
              alt="spicy"
            />
          </div>
        </div>
      </div>
    </main>
    <Footer />
  </div>
);

export default TermsAndContions;
