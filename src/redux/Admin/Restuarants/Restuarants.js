import axios from 'axios';
import ResStatus from './adminTypes'
import Swal from 'sweetalert2'

export const vendors = (vendors) => {
  return {
    type:ResStatus.ALLRestuarant,
    vendors
  }
}

export  const getAllRestuarant = (status, start) => {
  
  let url
  start === undefined ?  url = `${process.env.REACT_APP_BACKEND_URL}/vendors` : url = `${process.env.REACT_APP_BACKEND_URL}/vendors?page=${start}`
  
  return async dispatch => {
    try {
      await axios ({
        method: 'get',
        url:  url,
        headers: {
          'Content-Type': 'application/json'
        },
        timeout: 3000
      })
      .then( res => res.data)
      .then( response => {
        
        if(status) {
         const filterByStatus = response.data.filter( resStatus => resStatus.status === status )
         dispatch(vendors(filterByStatus))
        } else {
          dispatch(vendors(response.data))
        }
      })

    } catch (error) {
      console.log(error)
    }
  }
}

export const updateStatus = (item) => {
  let admin = localStorage.getItem('admin')
  let adminObj = JSON.parse(admin)
  const { id, updateTo } = item
  
  return async dispatch => {
    try {
      await axios({
        method: 'PATCH',
        url: `${process.env.REACT_APP_BACKEND_URL}/vendors/${id}/status`,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${adminObj.token}`
        },
        timeout: 3000,
        data: {status: updateTo}
      })
      .then(res => res.data)
      .then( response =>  {
        Swal.fire({
          icon: 'success',
          text: `Restuarant ${updateTo}`,
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          },
          showConfirmButton: false,
          timer: 1500
        })
        
      })

    } catch (error) {
      console.log(error)
    }
    
  
  }
}