import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import userReducer from "./user/userReducer";
import cartReducer from "./cartReducer/cartReducers";
import resturantReducer from "./restuarants/resturantReducers";
import orderReducers from "./orders/orderReducers";
import AdminReducer from './Admin/Restuarants/AdminReducer'

const persistConfig = {
  key: "root",
  storage,
  whitelist: ['cart', 'resturantReducer'],
};

const rootReducer = combineReducers({
  userReducer,
   resturantReducer,
  cart: cartReducer,
  orderReducers,
  AdminReducer,
});

export default persistReducer(persistConfig, rootReducer);
