import types from "./resturantTypes";

const INITIAL_STATE = {
  restuarant: {
    businessName: "",
    email: "",
    phone: "",
    password: "",
  },
  token: "",

  resloader: false,
  ressignuploader: false,
  isSignUpSuccessful: "false",
  getRestaurant: [],
  getRestaurantSuccess: false,
  getRestaurantFailure: false,
  currentRestaurant: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.RES:
  
      return {
        // ...state,
        currentRestaurant: action.currentRestuarant
        
      };
    case types.RES_SIGN_IN:
      return {
        ...state,
        resloader: action.payload,
      };

    case types.RES_SIGN_UP_LOADER:
      return {
        ...state,
        ressignuploader: action.payload,
      };

    case types.GET_RESTAURANT_START:
      return {
        ...state,
        getRestaurant: action.getRestaurant,
        resloader: true,
        getRestaurantSuccess: false,
        getRestaurantFailure: false,
      };
    case types.GET_RESTAURANT_SUCCESS:
      return {
        ...state,
        resloader: false,
        getRestaurantSuccess: action.getRestaurantSuccess,
      };

    case types.GET_RESTAURANT_FAILURE:
      return {
        ...state,
        resloader: false,
        getRestaurantFailure: action.getRestaurantFailure,
      };

    case types.GET_RESTAURANT_LOADING:
      return {
        ...state,
        resloader: action.resloader,
      };
    default:
      return state;
  }
};
