import types from './resturantTypes'
import axios from 'axios'
import Swal from 'sweetalert2'

const signinLoader = payload => {
  
  return {
    type: types.RES_SIGN_IN,
    payload
  }
}
const signupLoader = payload => {
  
  return {
    type: types.RES_SIGN_UP_LOADER,
    payload
  }
}

const restuarant = restuarant => {
  
  return {
    type: types.RES,
    // restuarant: restuarant.restuarant,
    token: restuarant.token,
    currentRestuarant: restuarant,
  }
}

export const restuarantSignUp = restaurant => {
  return async dispatch => {
    try {
      
      dispatch(signupLoader(true))
      await axios({
        method: 'post',
        url: 'https://platr-staging.herokuapp.com/auth/vendors/signup',
        // url: `${process.env.REACT_APP_BACKEND_URL}​/auth​/vendors​/signup`,
        headers: {
          'Content-Types': 'application/json'
        },
        data: restaurant,
        timeout: 4000
      })
      .then( response => response.data)
      .then( res => {
        if( res.status === 'success') {
          
          dispatch(signupLoader(false))
          dispatch(restuarant(res.data))
          Swal.fire({
            icon: 'success',
            text: `${res.status}, Thank you for showing interest in Platr, Check your email to proceed `,
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            showConfirmButton: false,
            timer: 4000
          })
          
              window.location.replace("/")
        }
      })
    } catch (error) {
      const err = JSON.stringify(error);
      const  errMessage = JSON.parse(err)

      const err1 = 'Request failed with status code 400'
      const err2 = 'Request failed with status code 409'
      
      if(errMessage.message === err1 ) {
          dispatch(signupLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'Verify your Credentials',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 4000
          })
      } 

      if(errMessage.message === 'Network Error' ) {
        dispatch(signupLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'Network Error',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 4000
          })
      } 

      if(errMessage.message === err2){
        dispatch(signupLoader(false))
        Swal.fire({
          icon: 'warning',
          text: 'Resturant name already exist',
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          },
          timer: 4000
        })
      }
      
      
    }
    }
  }

 const restuarantSignIn = restuarant => {
    return async dispatch => {
      try {
        
        dispatch(signinLoader(true))
        
        await axios({
          method: 'post',
          url: `${process.env.REACT_APP_BACKEND_URL}/auth/vendors/signin`,
          headers: {
            'Content-Type': 'application/json'
          },
          data: restuarant,
          timeout: 3000
        })
        .then( response => response.data)
        .then( res => {
          if( res.status === 'success') {
            dispatch(signinLoader(false))
            // dispatch(restuarant(res.data))
            Swal.fire({
              icon: 'success',
              text: `${res.status}, Welcome to platr`,
              showClass: {
                popup: 'animated fadeInDown faster'
              },
              hideClass: {
                popup: 'animated fadeOutUp faster'
              },
              showConfirmButton: false,
              timer: 1500
            })
            localStorage.setItem("restuarantsignin", JSON.stringify(res.data));
            window.location.replace("/")
          }
        })
      } catch (error) {
        const err = JSON.stringify(error)
        const errMessage = JSON.parse(err)
        
        const err1 = 'Request failed with status code 400'
        const err2 = 'Request failed with status code 401'
      
        if ( errMessage.message === err1) {
          dispatch(signinLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'Verify user input',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 1500
          })
        }
        if ( errMessage.message === err2) {
          dispatch(signinLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'Email or password incorrect',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 1500
          })
        }

      }
    }
  }


  export default restuarantSignIn 