import types from "./resturantTypes";
import axios from "axios";

export const getRestaurantStart = (payload) => {
  return {
    type: types.GET_RESTAURANT_START,
    getRestaurant: payload,
  };
};

export const getRestaurantSuccess = (getRestaurantSuccess) => {
  return {
    type: types.GET_RESTAURANT_SUCCESS,
    getRestaurantSuccess,
  };
};

export const getRestaurantFailure = (getRestaurantFailure) => {
  return {
    type: types.GET_RESTAURANT_SUCCESS,
    getRestaurantFailure,
  };
};

export const getRestaurantLoading = (resloader) => {
  return {
    type: types.GET_RESTAURANT_LOADING,
    resloader,
  };
};

export const getRestaurantActionSouth = () => {
  return async (dispatch) => {
    try {
      dispatch(getRestaurantLoading(true));

      setTimeout(
        await axios
          .get(
            `${process.env.REACT_APP_BACKEND_URL}/vendors?region=Kaduna%20South`
          )
          .then((response) => response.data)
          .then((restaurant) => {
            if (restaurant.status === "success") {
              dispatch(getRestaurantStart(restaurant.data));
              dispatch(getRestaurantSuccess(true));
            }
          }),
        5000
      );
    } catch (error) {
      dispatch(getRestaurantFailure(true));
    }
  };
};

export const getRestaurantActionNorth = () => {
  return async (dispatch) => {
    try {
      dispatch(getRestaurantLoading(true));

      setTimeout(
        await axios
          .get(
            `${process.env.REACT_APP_BACKEND_URL}/vendors?region=Kaduna%20North`
          )
          .then((response) => response.data)
          .then((restaurant) => {
            if (restaurant.status === "success") {
              dispatch(getRestaurantStart(restaurant.data));
              dispatch(getRestaurantSuccess(true));
            }
          }),
        5000
      );
    } catch (error) {
      dispatch(getRestaurantFailure(true));
    }
  };
};
