/* eslint-disable no-use-before-define */
import { createSelector } from 'reselect';



const selectCart = state => state.cart;

export const selectCartItems = createSelector(
    [selectCart],
    (cart) => cart.cartItems
);

export const selectCartItemsCount = createSelector(
    [selectCartItems],
    cartItems => cartItems.reduce(
        (accumulateQuantity, cartItem) => 
        accumulateQuantity + cartItem.quantity, 0)
);



export const selectCartTotal = createSelector(
    [selectCartItems],
    cartItems => cartItems.reduce(
        (accumulateQuantity, cartItem) => 
        accumulateQuantity + cartItem.quantity * cartItem.price + (cartItem.delivery * cartItem.quantity), 0)

);

// export const addQty = createSelector(
//     [addQty],
//     cartItems => cartItems.reduce(
//         (accumulateQuantity, cartItem) =>
//         accumulateQuantity * cartItem.price, 0)
// );
