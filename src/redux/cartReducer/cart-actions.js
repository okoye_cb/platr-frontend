import CartActionTypes from './cart.types';



export const toggleCartHidden = () => ({
    type: CartActionTypes.TOGGLE_CART_HIDDEN
});

export const addItem = item => ({
    type: CartActionTypes.ADD_ITEM,
    payload: item
})

export const removeItem = item =>({
    type: CartActionTypes.REMOVE_ITEM,
    payload: item

});


export const clearItemFromCArt = item => ({
    type: CartActionTypes.CLEAR_ITEM_FROM_CART,
    payload: item
});

export const qtyIncrement = item => ({
    type: CartActionTypes.INCREMENT,
    payload: item
});

export const qtyDecrement = item => ({
    type: CartActionTypes.DECREMENT,
    payload: item
});
