import ActionTypes from "./orderTypes";
import axios from "axios";

export const orderStatusStart = (payload) => {
  return {
    type: ActionTypes.ORDER_STATUS_START,
    orderData: payload,
  };
};

export const orderStatusSuccess = (orderStatusSuccess) => {
  return {
    type: ActionTypes.ORDER_STATUS_SUCCESS,
    orderStatusSuccess,
  };
};

export const orderStatusFailure = (orderStatusFailure) => {
  return {
    type: ActionTypes.ORDER_STATUS_FAILURE,
    orderStatusFailure,
  };
};

export const orderStatusLoading = (isLoading) => {
  return {
    type: ActionTypes.ORDER_STATUS_LOADING,
    isLoading,
  };
};

export const orderConfirmationAction = (orderId) => {
  return async (dispatch) => {
    try {
      dispatch(orderStatusLoading(true));
      await axios
        .get(`${process.env.REACT_APP_BACKEND_URL}/orders/status/${orderId}`)
        .then((response) => response.data)
        .then((res) => {
          if (res.status === "success") {
            dispatch(orderStatusStart(res.data.order));
            dispatch(orderStatusSuccess(true));
          }
        });
    } catch (error) {
      dispatch(orderStatusFailure(true));
    }
  };
};
