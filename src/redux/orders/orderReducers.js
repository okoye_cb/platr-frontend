import ActionTypes from "./orderTypes";

const INITIAL_STATE = {
  orderData: null,
  isLoading: false,
  orderStatusSuccess: false,
  orderStatusFailure: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ActionTypes.ORDER_STATUS_START:
      return {
        ...state,
        orderData: action.orderData,
        isLoading: true,
        orderStatusFailure: false,
        orderStatusSuccess: false,
      };

    case ActionTypes.ORDER_STATUS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        orderStatusSuccess: action.orderStatusSuccess,
      };

    case ActionTypes.ORDER_STATUS_FAILURE:
      return {
        ...state,
        isLoading: false,
        orderStatusFailure: action.orderStatusFailure,
      };

    case ActionTypes.ORDER_STATUS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    default:
      return state;
  }
};
