import AuthMethod from './userType'

const INITIAL_STATE = {
  user: {
    createdAt: '',
    updated: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    password: '',
    confirmPassword: '',
    id: '',
    address: '',
    isAdmin: ''

  },
  token: '',

  loader: false,
  signinloader: false,
  isSignUpSuccessful: 'false'

}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AuthMethod.SIGN_UP_LOADER:
      return {
        ...state,
        loader: action.loader
      };

    case AuthMethod.SIGN_IN_LOADER:
      return {
        ...state,
        signinloader: action.payload
      }

    case AuthMethod.USER:
      return {
        ...state,
        user: action.user,
        token: action.token,
      };

    case AuthMethod.IS_SIGNUP_SUCCESSFULL:
      
      return {
        ...state,
        isSignUpSuccessful: action.payload
      }

    default:
      return state;

  }

}