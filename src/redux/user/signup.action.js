import  AuthMethod from './userType'
import axios from 'axios'
import Swal from 'sweetalert2'

export const UserData = userdata => {
  return {
    type:  AuthMethod.USER,
    user:  userdata.user,
    token: userdata.token
  }
}

export const userSignUp = userData => {

  return {
    type: AuthMethod.USER_SIGN_IN,
    email: userData.email,
    password: userData.password
  }
}

export const Loading = loader => {
  return {
    type: AuthMethod.SIGN_UP_LOADER,
    loader
  }
}

export const SignInLoader = payload => {
  return {
    type: AuthMethod.SIGN_IN_LOADER,
    payload
  }
}

export const isSignupSuccessfull =  payload => {
  return {
    type: AuthMethod.IS_SIGNUP_SUCCESSFULL,
    payload
  }
}


export const signUpWithEmailAndPassword =  (userCred) =>  {
  return async dispatch => {
    const { password, confirmPassword } = userCred
    
    if(password === confirmPassword){
      try {
        
        dispatch(Loading(true))
        await axios({
          method: 'post',
           url: `${process.env.REACT_APP_BACKEND_URL}/auth/customers/signup`,

          headers: {
            'Content-Type': 'application/json'
          },
          data: userCred,
          timeout: 3000
          })
          .then( response => response.data )
          .then( res => {
            
            if(res.status === 'success'){
              dispatch(Loading(false))
              dispatch(isSignupSuccessfull(true))
              dispatch(UserData(res.data))
              Swal.fire({
                icon: 'success',
                text: `${res.status}, Welcome to platr`,
                showClass: {
                  popup: 'animated fadeInDown faster'
                },
                hideClass: {
                  popup: 'animated fadeOutUp faster'
                },
                showConfirmButton: false,
                timer: 1500
              })
              
              localStorage.setItem("user", JSON.stringify(res.data));
              window.location.replace("/")
              
            }
            
            
          })
      } catch (error) {
        const err = JSON.stringify(error);
        const  errMessage = JSON.parse(err)

        const err1 = 'Request failed with status code 400'
        const err2 = 'Request failed with status code 409'
        
        if(errMessage.message === err1 ) {
            dispatch(Loading(false))
            Swal.fire({
              icon: 'warning',
              text: 'fill in all user input',
              showClass: {
                popup: 'animated fadeInDown faster'
              },
              hideClass: {
                popup: 'animated fadeOutUp faster'
              },
              timer: 1500
            })
        } 

        if(errMessage.message === 'Network Error' ) {
            dispatch(Loading(false))
            Swal.fire({
              icon: 'warning',
              text: 'Network Error',
              showClass: {
                popup: 'animated fadeInDown faster'
              },
              hideClass: {
                popup: 'animated fadeOutUp faster'
              },
              timer: 1500
            })
        } 

        if(errMessage.message === err2){
          dispatch(Loading(false))
          Swal.fire({
            icon: 'warning',
            text: 'User already exist',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 1500
          })
        }
        
        
      }

    } else {
      dispatch(Loading(true))
      Swal.fire({
        icon: 'info',
        text: 'password Mismatch',
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        },
        timer: 2000,
        showConfirmButton: false
      })
      dispatch(Loading(false))
    }


  }
}

export const  resetPassword = ( emailObj ) => {
  return async dispatch => {
    try {
      
      
      await axios ({
        method: 'post',
        url: `${process.env.REACT_APP_BACKEND_URL}/auth/reset_password`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: emailObj,
        timeout: 3000
      })
      .then( res => res.data)
      .then( response => {
        dispatch(SignInLoader(false))
        Swal.fire({
          icon: 'success',
          text: `${response.status}, Check your email to continue the process`,
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          },
          showConfirmButton: false,
          timer: 3000
        })
        window.location.replace("/")
      })
    } catch (error) {
      
      const err = JSON.stringify(error);
      const  errMessage = JSON.parse(err)
      const err1 = 'Request failed with status code 404'
      const err2 = 'Request failed with status code 400'
        
      
        if ( errMessage.message ===  err2) {
          dispatch(SignInLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'Verify Email',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 3000
          })
        }
        if ( errMessage.message ===  err1) {
          dispatch(SignInLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'User doesn\'t exist',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 3000
          })
        }
    }
  }
}

export const signInWithEmailAndPassword = (user) => {
  return async dispatch => {
    try {
      dispatch(Loading(false))
      dispatch(SignInLoader(true))
      await axios({
        method: 'post',
        url: `${process.env.REACT_APP_BACKEND_URL}/auth/customers/signin`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: user,
        timeout: 3000
      })
      .then( res => res.data)
      .then(response => {
      
        if ( response.status === 'success' && response.data.userDetails.isAdmin  ) {

        dispatch(SignInLoader(false))
        Swal.fire({
          icon: 'success',
          text: `${response.status}, Welcome to platr`,
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          },
          showConfirmButton: false,
          timer: 1500
        })
        localStorage.setItem("admin", JSON.stringify(response.data));
        window.location.replace("/admin")
      }

        if( response.status === 'success' && !response.data.userDetails.isAdmin ){
          dispatch(SignInLoader(false))
          Swal.fire({
            icon: 'success',
            text: `${response.status}, Welcome to platr`,
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            showConfirmButton: false,
            timer: 1500
          })
          localStorage.setItem("userdata", JSON.stringify(response.data));
          window.location.replace("/")
        } 

         
        
        
      })
    } catch (error) {
      const err = JSON.stringify(error);
        const  errMessage = JSON.parse(err)

        const err1 = 'Request failed with status code 400'
        const err2 = 'Request failed with status code 401'
      
        if ( errMessage.message === err1) {
          dispatch(SignInLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'Verify user input',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 1500
          })
        }
        if ( errMessage.message === err2) {
          dispatch(SignInLoader(false))
          Swal.fire({
            icon: 'warning',
            text: 'Email or password incorrect',
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            },
            timer: 1500
          })
        }
    }
  }
}

