const AuthMethod = {
  SIGN_UP_LOADER: 'signuploader',
  SIGN_IN_LOADER: 'signinloader',
  USER: 'user',
  IS_SIGNUP_SUCCESSFULL: 'issignupsuccessfull',
  USER_SIGN_IN: 'usersignin'
}

export default AuthMethod
