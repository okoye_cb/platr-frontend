import firebase from 'firebase/app'
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAefCxjkf4d0kc1-I5V0SbujuBogWJ8tKY",
  authDomain: "platr-a22e4.firebaseapp.com",
  databaseURL: "https://platr-a22e4.firebaseio.com",
  projectId: "platr-a22e4",
  storageBucket: "platr-a22e4.appspot.com",
  messagingSenderId: "1010157249516",
  appId: "1:1010157249516:web:784f03a1f36b32b65cd6c9",
  measurementId: "G-YTH1QVWJFG",
};



firebase.initializeApp(firebaseConfig);


export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const userId = auth.currentUser;




//Google firebase Auth
export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(googleProvider);

//Facebook firebase Auth
export const facebookProvider = new firebase.auth.FacebookAuthProvider();
facebookProvider.setCustomParameters({ prompt: 'select_account'})
export const signInWithFacebook = () => auth.signInWithPopup(facebookProvider)


//Sign Out
export const doSignOut = () => auth.signOut();

export default firebase;
