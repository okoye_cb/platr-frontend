import React from "react";
import { Link } from "react-router-dom";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";

import "./footer.scss";

const Footer = () => (
  <div className="footer">
    <div className="content">
      {/* <section className="ha">
        <h3 className="head">our work</h3>
        <div className="line"></div>
        <ul className="list">
          <Link to="/restaurants">
            <li className="item">Resturants</li>
          </Link>
          <Link to="">
            <li className="item">Deliver Food</li>
          </Link>
          <Link to="">
            <li className="item">Delivery Station</li>
          </Link>
        </ul>
      </section> */}
      <section className="b">
        {/* <h3 className="about">About us</h3> */}
        <ul className="list">
          {/*<div className="line"></div>
          <Link to="/faq">
            <li className="item">FAQ</li>
          </Link>
           <Link to="/blog">
            <li className="item"> Blog</li>
          </Link>
          <Link to="/about">
            <li className="item">About</li>
          </Link> */}
        </ul>
      </section>
      <section>
        {/* <h3>Contact Us</h3> 
        <div className="line"></div>*/}
        <div  className="c">
        <div>
          <Link to="/contact">
            <li className="item"> Contact Us</li>
          </Link>
        </div>
        <div>
          <Link to="/terms">
            <li className="item">Terms of Use</li>
          </Link>
        </div>
        <div>
          <Link to="/privacy">
            <li className="item">Privacy Policy</li>
          </Link>
        </div>
        <div>
          <Link to="/faq">
            <li className="item">FAQ</li>
          </Link>
        </div>
        </div>

        {/* <address>
          No 1 Udoette street, D-line,
          <br />
          Uyo,
          <br />
          Akwa Ibom state
        </address> */}<h4 className="social-title">Follow Us</h4>
        <div className="social-media">
          
          <div className="social-icon">
<FacebookIcon color="primary" fontSize="large"/>
          </div>
          <div className="social-icon">
            <TwitterIcon style={{ color: '#00acee' }} fontSize="large" />
          </div>
          <div className="social-icon">
            <InstagramIcon style={{ color: '#3f729b' }} fontSize="large"/>
          </div>
        
               
      </div>
      </section>
      
      <section className="subscribe">
        <h3>SUBSCRIBE TO OUR NEWSLETTER</h3>
        <p>
          Stay up to date with our activities by subcribing to our newsletter.
        </p>
        <div className="w">
          <input
            className="mlg"
            placeholder="Please input your email address"
          />
          <button className="b">Subscribe Now</button>
        </div>
      </section>
    </div>

    <div className="right">
      <p>Copyright 2019. Platr All Right Observed </p>
      {/* <span>
        <Link to="/faq">FAQ</Link>
        <span className="space">|</span>
        <Link to="/help">Help</Link>
      </span> */}
    </div>
  </div>
);
export default Footer;
