import React from "react";
import { PropTypes } from "prop-types";
import "./formInput.styles.scss";

const FormInput = ({ placeholder, label, className, ...otherProps }) => (
  <div className="form-group">
    <label>{label}</label>
    <input placeholder={placeholder} {...otherProps} className={className} />
  </div>
);

FormInput.propTypes = {
  placeholder: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  label: PropTypes.string
};
export default FormInput;
