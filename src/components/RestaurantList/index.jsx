import React from "react";
import { PropTypes } from "prop-types";
import ReactStarRating from "react-star-ratings-component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faCar,
  faShoppingCart,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import "./restaurant.styles.scss";

const RestaurantListCard = ({
  imageUrl,
  RestaurantName,
  RestaurantDishes,
  PrepTime,
  DeliveryTime,
  price,
}) => (
  <Link to="/restaurant" style={{ textDecorationLine: "none" }}>
    <div className="containers">
      <div className="row">
        <div className="col-lg-3 col-md-3 col-sm-12 img-wrap">
          <img
            src={imageUrl}
            alt="restaurant_img"
            className="img-responsive image"
          />
        </div>
        <div className="col-lg-9 col-md-9 col-sm-12 content-wrap">
          <div className="wrap__items  top">
            <h5 className="h5">{RestaurantName}</h5>
            <h6 className="h6">{RestaurantDishes}</h6>
            <div className="star">
              <div className="star__items">
                <ReactStarRating
                  numberOfStar={5}
                  numberOfSelectedStar={4}
                  colorEmptyStar="#cfcfcf"
                />
              </div>
              <div
                className="star__items"
                style={{
                  fontSize: ".5em",
                  marginTop: "1.3em",
                  color: "rgba(102, 102, 102, 0.6)",
                }}
              >
                (396)
              </div>
            </div>
          </div>
          <div className="wrap__items bottom">
            <p className="para">
              <FontAwesomeIcon icon={faClock} size="lg" color="gray" />{" "}
              &nbsp;&nbsp;
              {PrepTime}
            </p>
            <p className="para">
              <FontAwesomeIcon icon={faCar} size="lg" color="gray" />
              &nbsp;&nbsp;
              {DeliveryTime}
            </p>
            <p className="para">
              <FontAwesomeIcon icon={faShoppingCart} size="lg" color="gray" />
              &nbsp;&nbsp; Min. Order &mdash;{price}
            </p>
          </div>
        </div>
      </div>
    </div>
  </Link>
);

RestaurantListCard.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  RestaurantName: PropTypes.string.isRequired,
  RestaurantDishes: PropTypes.string.isRequired,
  prepTime: PropTypes.string.isRequired,
  DeliveryTime: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
};

export default RestaurantListCard;
