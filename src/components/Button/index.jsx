import React from "react";
import PropTypes from "prop-types";
import "./button.styles.scss";

const CustomButton = ({ text, icon, size, ...otherProps }) => (
  <React.Fragment>
    <div className="btn">
      <button className={size} {...otherProps}>
        {text} {icon}
      </button>
    </div>
  </React.Fragment>
);

CustomButton.propTypes = {
  text: PropTypes.string.isRequired,
  size: PropTypes.string
};
export default CustomButton;
