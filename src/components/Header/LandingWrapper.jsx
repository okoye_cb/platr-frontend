import React, { Component } from "react";
import "./landingwrapper.scss";
import "bootstrap/dist/css/bootstrap.css";
import NewNav from "./newNavBar";
import Selector from "../PagesHeader/Selector";
import platr from "../../platr.svg";

class LandingWrapper extends Component {
  constructor(props){
    super()
    
  }
  render() {
    
    return (
      <div className="container-fluid landingpage--header">
        <NewNav
          idName="pink-color"
          brand={platr}
          bgColor="white"
          iconColor="icon-pink"
          
        />
        <h2 className="landingpage--header__title tatle">
          Eat Fresh, Eat with Convenience
        </h2>
        <p className="landingpage--header__text">“One cannot think well, love well, sleep well, if one has not dined well.”
        <br/> ― Virginia Woolf, A Room of One's Own
        </p>
        <Selector />
      </div>
    );
  }
}



export default LandingWrapper;
