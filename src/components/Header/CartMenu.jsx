import React from 'react';
import './navigation.scss';
import { connect } from 'react-redux';
import { toggleCartHidden } from '../../redux/cartReducer/cart-actions'



const CartMenu = ({toggleCartHidden}) => (
    
    <div onClick={toggleCartHidden}>
        <p  className="menu-link" to="/cart">
              Cart <span><sup className="cart-qty">0</sup></span>
            </p>
    </div>


)

const mapDispatchToProps = dispatch => ({
    toggleCartHidden: () => dispatch(toggleCartHidden())
});

export default connect(null, mapDispatchToProps) (CartMenu);