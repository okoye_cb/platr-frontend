import React, { useEffect } from "react";
import clsx from "clsx";
import { makeStyles, useTheme, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import LocalMallOutlinedIcon from '@material-ui/icons/LocalMallOutlined';
import OrderStatusModal from "../Modal/OrderStatus/index";
import { Link } from "react-router-dom";
import styled from "styled-components";
import "./navigation.scss";
import { NavDropdown } from "react-bootstrap";
import { auth } from "../../firebase/firebaseConfig";
import { selectCartItemsCount } from '../../redux/cartReducer/cart.selectors'
import { connect } from "react-redux";
import axios from 'axios';


const drawerWidth = 240;
const AuthSection = styled.div`
  display: flex;
  flex-direction: column;
`;
const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    paper: {
      background: "black",
      color: "white",
    },
    appBar: {
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginRight: drawerWidth,
    },
    title: {
      flexGrow: 1,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerHeader: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: "flex-start",
    },
  })
);

const PersistentDrawerRight = (props) => {
  
  const {restuarant : { currentRestaurant }} = props
  const  { restaurant } = currentRestaurant

  const {
    idName,
    name,
    link,
    itemCount,
    brand,
    home,
    text,
    bgColor,
    iconColor,
  } = props;
  const [modalShow, setModalShow] = React.useState(false);

 
  const test = window.localStorage.getItem("currentUser");
  const newTest = JSON.parse(test);

  const userJSON = localStorage.getItem("user");
  const userObj = JSON.parse(userJSON);
  
  const usersigninJSON = localStorage.getItem("usersignin");
  const usersigninObj = JSON.parse(usersigninJSON);


  const resDetail = localStorage.getItem('restuarantsignin')
  const resDetailObj = JSON.parse(resDetail)

  const shortenName = (obj)=> {
      if(obj === [])  return;
    let newRes = {...obj};
  
    const {restaurant} = newRes
    const { businessName } = restaurant
  
    const arrName = businessName.split(" ")
    const name = []

    // eslint-disable-next-line array-callback-return
    arrName.map( item => {
        name.push(item.charAt(0))
      
    })

    return `Welcome, ${name.join('').toUpperCase()}`
  }


  //Sign Out
  const signOut = () => {
    localStorage.clear();
    auth.signOut();
    window.location.replace("/");
  };
  //Sign Out

  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const log = () => {
    const getToken = async () => {

      const Token = props.token;
      console.log(Token)
      try {
        await axios({
          method: 'get',
          url:`${process.env.REACT_APP_BACKEND_URL}/vendors/profile/me`,
          header: {
            'Content-Types': 'application/json',
            'Authorization':  `Bearer ${Token}`
          },
          timeout: 3000
        })
        .then( res => res.data)
        .then( response =>  localStorage.setItem('restuarant', JSON.stringify(response)))

      } catch (error) {
        console.log(error)
      }
    }
    getToken()
    }

    useEffect(() => {
      return log()
    }, [])

  return (
    <div className={classes.root}>
      <OrderStatusModal show={modalShow} onHide={() => setModalShow(false)} />

      <CssBaseline />
      <AppBar
        // position="static"
        color={bgColor}
        style={{ boxShadow: "1" }}
      >
        <Toolbar>
          <Typography variant="h6" noWrap className={classes.title}>
            <Link to="/" id={idName} className="brand-logo">
              <img src={brand} alt="logo" />
            </Link>
          </Typography>
          <List id="hide-list">
            <Link id={idName} className="menu-link" to={home}>
              {text}
            </Link>
            {/* <Link id={idName} className="menu-link" to="/about">
              About Us
            </Link> */}

            <Link id={idName} className="menu-link" to='/cart'>
          <LocalMallOutlinedIcon style={{ fontSize: 40 }} /> <span className="cart-qty">{ itemCount }</span>
            </Link>
            <Link id={idName} className="menu-link to-hide" to={link}>
              {name}
            </Link>
           
          </List>

          <NavDropdown
            title={
              test
                ? `Hi, ${newTest.name}`
                : usersigninObj
                ? `Hi, ${usersigninObj.userDetails.firstName}`
                : userObj
                ? `Hi, ${userObj.user.firstName}`
                : restaurant  ? 
                shortenName(currentRestaurant)
                : resDetailObj ?
                shortenName(resDetailObj) 
                :"Account"
            }
            id={idName}
            className="d-down"
          >
            {test || userJSON || usersigninObj || restaurant  || resDetail ? (
              <div>
                <NavDropdown.Item href='/user'>Account</NavDropdown.Item>
                <NavDropdown.Item
                  onClick={() => {
                    setModalShow(true);
                  }}
                >
                  Order status
                </NavDropdown.Item>
                <Divider />
                <NavDropdown.Item onClick={signOut}>Sign Out</NavDropdown.Item>
              </div>
            ) : (
              <div>
                <NavDropdown.Item href="/auth">Sign In/Up</NavDropdown.Item>
                <NavDropdown.Item href="/Rauth">Resturant LogIn</NavDropdown.Item>
                <NavDropdown.Item
                  onClick={() => {
                    setModalShow(true);
                  }}
                >
                  Order status
                </NavDropdown.Item>
              </div>
            )}
          </NavDropdown>
          <IconButton
            id={iconColor}
            color="inherit"
            aria-label="open drawer"
            edge="end"
            onClick={handleDrawerOpen}
            className={clsx(open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="right"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          <AuthSection>
            <ListItem>
              <h3 id={idName} style={{ fontSize: "18px", fontWeight: "bold" }}>
                {test
                  ? `Hi, ${newTest.name}`
                  : usersigninObj
                  ? `Hi, ${usersigninObj.userDetails.firstName}`
                  : userObj
                  ? `Hi, ${userObj.user.firstName}`
                  :  restaurant ? 
                  shortenName(currentRestaurant)
                  : resDetailObj ?
                  shortenName(resDetailObj) 
                  :"Account"
                 }
              </h3>
            </ListItem>
            {test || userJSON || usersigninObj || resDetail || restaurant ? (
              <div>
                <ListItem>
                  <Link id={idName} to='/user'>Account</Link>
                </ListItem>
                <ListItem>
                  <Link
                    id={idName}
                    onClick={() => {
                      setModalShow(true);
                      setOpen(false);
                    }}
                  >
                    Order status
                  </Link>
                </ListItem>
                <ListItem>
                  <Link id={idName} onClick={signOut}>
                    Sign Out
                  </Link>
                </ListItem>
              </div>
            ) : (
              <div>
                <ListItem>
                  <Link id={idName} to="/auth">
                    Sign In/Up
                  </Link>
                </ListItem>
                <ListItem>
                  <Link id={idName} to="/rauth">
                    Resturant Log in
                  </Link>
                </ListItem>
                <ListItem>
                  <Link
                    id={idName}
                    onClick={() => {
                      setModalShow(true);
                      setOpen(false);
                    }}
                  >
                    Order status
                  </Link>
                </ListItem>
              </div>
            )}
          </AuthSection>
        </List>

        <Divider />
        <List>
          <ListItem>
            <Link id={idName} to={home}>
              {text}
            </Link>
          </ListItem>
          {/* <ListItem>
            <Link id={idName} to="/about">
              About Us
            </Link>
          </ListItem> */}

          <ListItem>
            <Link id={idName} to='/cart'>
        <LocalMallOutlinedIcon style={{ fontSize: 40 }} /> <span className="cart-qty">{itemCount}</span>
            </Link>
          </ListItem>
          <ListItem>
            <Link id={idName} to={link}>
              {name}
            </Link>
          </ListItem>
        </List>
      </Drawer>
    </div>
  );
};


  const mapStateToProps = (state ) => ({
    user: state.userReducer,
    itemCount: selectCartItemsCount(state),
    restuarant: state.resturantReducer
});

export default connect(mapStateToProps, null)(PersistentDrawerRight);
