import React from "react";
import "./navigation.scss";
import { Navbar, NavDropdown, Nav } from "react-bootstrap";
// import OrderStatusModal from "../Modal/OrderStatus";
import "bootstrap/dist/css/bootstrap.css";
import { Link } from "react-router-dom";
const Navigation = ({
  idName,
  name,
  link,
  cartOrBlogLink,
  cartTitleOrBlog,
  brand,
  home,
  text,
}) => {
  // const [modalShow, setModalShow] = React.useState(false);
  return (
    <React.Fragment>
      <>
        {/* <OrderStatusModal show={modalShow} onHide={() => setModalShow(false)} /> */}
      </>
      <div className="container">
        <Navbar expand="sm">
          <Link to="/" id={idName} className="brand-logo">
            <img src={brand} alt="platr" />
          </Link>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav id="menu-link-group">
              <Link id={idName} className="menu-link" to={home}>
                {text}
              </Link>
              <Link id={idName} className="menu-link" to="/about">
                About Us
              </Link>

              <Link id={idName} className="menu-link" to={cartOrBlogLink}>
                {cartTitleOrBlog}
              </Link>
              <Link id={idName} className="menu-link to-hide" to={link}>
                {name}
              </Link>
            </Nav>{" "}
            <NavDropdown title="Account" id={idName} className="d-down">
              <NavDropdown.Item href="/auth">SignIn/Up</NavDropdown.Item>
              <NavDropdown.Item>Sign Out</NavDropdown.Item>
              {/* <NavDropdown.Item onClick={() => setModalShow(true)}>
                Order status
              </NavDropdown.Item> */}
            </NavDropdown>
          </Navbar.Collapse>
        </Navbar>
      </div>
    </React.Fragment>
  );
};

export default Navigation;

// import React from "react";
// import "./navigation.scss";
// import { Navbar, NavDropdown, Nav } from "react-bootstrap";
// import OrderStatusModal from "../Modal/OrderStatus";
// import "bootstrap/dist/css/bootstrap.css";
// import { Link } from "react-router-dom";
// import platr from '../../platr.svg'
// class Navigation extends React.Component{
//   state={
//   idName: '',
//   name: '',
//   link: '',
//   cartOrBlogLink: '',
//   cartTitleOrBlog:'',
//   brand: '',
//   home: '',
//   text: ''
// }
// render(){
//   // const [modalShow, setModalShow] = React.useState(false);

//   return(
// <React.Fragment>
//       <>
//         {/* <OrderStatusModal show={modalShow} onHide={() => setModalShow(false)} /> */}
//       </>
//       <div className="container">
//         <Navbar expand="sm">
//           <Link to="/" className="brand-logo">
//             <img src={platr} alt="platr" />
//           </Link>
//           <Navbar.Toggle aria-controls="basic-navbar-nav" />
//           <Navbar.Collapse id="basic-navbar-nav">
//             <Nav id="menu-link-group">
//               <Link className="menu-link" to="/">
//                 Home
//               </Link>
//               <Link className="menu-link" to="/about">
//                 About Us
//               </Link>

//               <Link className="menu-link" to="/">
//                 Blog
//               </Link>
//               <Link className="menu-link to-hide" to= "/">
//                 Hello
//               </Link>
//             </Nav>{" "}
//             <NavDropdown title="Account" className="d-down">
//               <NavDropdown.Item href="/auth">SignIn/Up</NavDropdown.Item>
//               <NavDropdown.Item>Sign Out</NavDropdown.Item>
//               {/* <NavDropdown.Item onClick={() => setModalShow(true)}>
//                 Order status
//               </NavDropdown.Item> */}
//             </NavDropdown>
//           </Navbar.Collapse>
//         </Navbar>
//       </div>
//     </React.Fragment>
//   )
// }
// }

// export default Navigation;
