import React, { useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Modal from "react-bootstrap/Modal";
import ReactStarRating from "react-star-ratings-component";
import { orderConfirmationAction } from "../../../redux/orders/orderActions";

import "./OrderStatus.scss";

function OrderStatusModal(props) {
  const {
    orderConfirmationAction,
    orderData,
    isLoading,
    orderStatusSuccess,
    orderStatusFailure,
  } = props;
  const [orderId, setOrderId] = useState("");
  const [error, setError] = useState(false);

  const handleChange = (event) => {
    const { value } = event.target;
    setOrderId(value);
  };

  const regexCheck = /[ !"#$%&'()*+,./:;<=>?@[\\\]^_`{|}~]/g.test(orderId);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (regexCheck || orderId === "") {
      setError(true);
      return;
    }
    orderConfirmationAction(orderId);
    setOrderId("");
    setError(false);
  };

  const statusSwitch = orderStatusSuccess
    ? "alert alert-success"
    : "alert alert-danger";

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12">
              <div className="modal-wrapper">
                <h4 className="h4">Order Status</h4>
                {error && (
                  <span className="alert alert-warning">
                    Please enter a valid Order ID
                  </span>
                )}
                {orderStatusSuccess || orderStatusFailure ? (
                  <div className={`text-center ${statusSwitch}`}>
                    <strong>
                      Status:{" "}
                      {orderData
                        ? orderData.status.toUpperCase()
                        : "Order not found!"}
                    </strong>
                  </div>
                ) : (
                  <div>
                    <p className="tagline">
                      Check your order status. View a summary of your order and
                      processing status
                    </p>
                    <p className="tagline">
                      Review delivery information. View and confirm delivery
                      information. Your delivery selection can be updated online
                      until we receive your application.
                    </p>
                  </div>
                )}

                <form action="" className="modal-form">
                  <input
                    type="text"
                    placeholder="Order Number"
                    required
                    className="modal-input"
                    name="orderId"
                    value={orderId}
                    onChange={handleChange}
                  />
                  {isLoading ? (
                    <button className="btn-modal" disabled>
                      <div className="order-loader"></div>
                    </button>
                  ) : (
                    <button className="btn-modal" onClick={handleSubmit}>
                      Check Status
                    </button>
                  )}
                </form>

                <div className="review">
                  <h4 className="review-h4">Leave a Review</h4>
                  <ReactStarRating numberOfStar={5} colorEmptyStar="#cfcfcf" />
                  <form action="" className="review-form">
                    <input
                      type="text"
                      placeholder="Write your name here"
                      required
                      className="review-input"
                    />
                    <textarea
                      className="review-text"
                      cols="5"
                      rows="5"
                      placeholder="Write your review here"
                    ></textarea>
                    <button className="btn-review">Submit Review</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

const mapStateToProps = (state) => {
  const {
    orderData,
    isLoading,
    orderStatusSuccess,
    orderStatusFailure,
  } = state.orderReducers;
  return {
    orderData,
    isLoading,
    orderStatusFailure,
    orderStatusSuccess,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      orderConfirmationAction: orderConfirmationAction,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderStatusModal);
