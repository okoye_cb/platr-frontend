const Data = [
  {
    id: 1,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068266/landingPage/Rectangle_3.35_gojtwg.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  },
  {
    id: 2,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068265/landingPage/Rectangle_3.35_1_mjq5fp.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  },
  {
    id: 3,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068265/landingPage/Rectangle_3.35_2_r83kfg.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  },
  {
    id: 4,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068265/landingPage/Rectangle_3.35_2_r83kfg.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  },
  {
    id: 5,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068266/landingPage/Rectangle_3.35_gojtwg.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  },
  {
    id: 6,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068265/landingPage/Rectangle_3.35_1_mjq5fp.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  },
  {
    id: 7,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068266/landingPage/Rectangle_3.35_gojtwg.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  },
  {
    id: 8,
    imageUrl:
      "https://res.cloudinary.com/doo1zd8pn/image/upload/v1580068265/landingPage/Rectangle_3.35_2_r83kfg.png",
    headText: "Platr Food Blog",
    benefitText: "  Health Benefit of the most wanted Waffles",
    article:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit Suntasperiores maxime eum fugiat deleniti voluptatum odit animi,perspiciatis, nesciunt debitis quam iure.Ipsa nesciunt, dolor voluptas placeat facilis fuga sit."
  }
];

export default Data;
