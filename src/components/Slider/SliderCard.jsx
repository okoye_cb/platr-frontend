import React from "react";
import "./slider.styles.scss";

const SliderCard = ({ imgUrl, headText, benefitText, article }) => (
  <section className="slider-item" style={{ width: "90%" }}>
    <div className="image">
      <img src={imgUrl} alt="" className="img" />
    </div>
    <div className="content">
      <h4 className="head-text">{headText}</h4>
      <p className="benefit-text">{benefitText}</p>
      <article className="article">{article}</article>
    </div>
  </section>
);

export default SliderCard;
