import React, { Component } from "react";
import Slider from "react-slick";
import SlideCard from "./SliderCard";
import Data from "./data";
import "./slick.css";
import "./slick-theme.css";

export default class Responsive extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 300,
      autoplay: true,
      slidesToShow: 3,
      slidesToScroll: 4,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <div style={{ overflow: "hidden" }}>
        <Slider {...settings}>
          {Data.map(item => (
            <div key={item.id}>
              <SlideCard
                headText={item.headText}
                imgUrl={item.imageUrl}
                benefitText={item.benefitText}
                article={item.article}
              />
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}
