import React from "react";
import "./selector.style.scss";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'
import {Link} from "react-router-dom"


class Selector extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isOpen: false };
    this.toggleContainer = React.createRef();

    this.onClickHandler = this.onClickHandler.bind(this);
    this.onClickOutsideHandler = this.onClickOutsideHandler.bind(this);
  }

  componentDidMount() {
    window.addEventListener("click", this.onClickOutsideHandler);
  }

  componentWillUnmount() {
    window.removeEventListener("click", this.onClickOutsideHandler);
  }

  onClickHandler() {
    this.setState(currentState => ({
      isOpen: !currentState.isOpen
    }));
  }

  onClickOutsideHandler(event) {
    if (
      this.state.isOpen &&
      !this.toggleContainer.current.contains(event.target)
    ) {
      this.setState({ isOpen: false });
    }
  }

  render() {
    return (
      <div ref={this.toggleContainer}>
        <button id="region--selector" onClick={this.onClickHandler}>
          <span>Select your city  <FontAwesomeIcon id="region--selector__caret" icon={faAngleDown} /></span>
         
        </button>
        {this.state.isOpen && (
 
             <ul id="region--selector__area">

               <Link to="/kaduna-north" style={{textDecoration: 'none'}}>
               <li>
              {" "}
              <Link id="selector" to="/kaduna-north">
                Kaduna North
              </Link>
            </li>
               </Link>

               <Link to="/kaduna-south" style={{textDecoration: 'none'}}>
               <li>
              <Link id="selector" to="/kaduna-south">
                Kaduna South
              </Link>
            </li>
               </Link>
           


            
          </ul>
   
         
        )}
      </div>
    );
  }
}

export default Selector;
