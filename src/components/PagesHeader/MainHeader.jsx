import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "./mainheader.scss";
import propTypes from "prop-types";

const MainHeader = props => {
  return (
    <div className="container-fluid main--header">{props.renderProp()}</div>
  );
}

MainHeader.propTypes = {
  renderProp: propTypes.func
};

export default MainHeader;